
package motifGenerator;

import java.util.*;

public class DotReactionClasses {
  
    private HashMap<HashSet<Complex>,LinkedList<String>> hm;
    
    public DotReactionClasses(){
      hm = new HashMap<HashSet<Complex>,LinkedList<String>>();
    }
    
    public void add(Complex A, Complex B, String reaction){
      HashSet<Complex> key = new HashSet<Complex>();
      key.add(A);
      key.add(B);
      LinkedList<String> lls = hm.get(key);
      if (lls != null){
        //pair of Complex already in the map, add new string
        lls.add(reaction);
      } else {
        //new map
        lls = new LinkedList<String>();
        lls.add(reaction);
        hm.put(key,lls);
      }
    }
    
    public String printDOT_ReactionsInSameRank(){
      
      String returnString = "";
      Iterator<LinkedList<String>> iteLLS = this.hm.values().iterator();
      while (iteLLS.hasNext()){
        LinkedList<String> lls = iteLLS.next();
        returnString += "{ rank=same; ";
        
        Iterator<String> iteS = lls.iterator();
        while (iteS.hasNext()){
          returnString += iteS.next() + "; ";
        }
        
        returnString += "};\n";
      }
      return returnString;
    }
}
