
package motifGenerator;

import java.util.*;

/**
 * Class for the reaction A -> B + C
 * */

public class UnBindingReaction extends Reaction{
  
  private Complex A;
  private Complex B;
  private Complex C;
  
  /**
   * Constructor stores pointers to the Complex objects. Attributes of
   * the Complex objects can be altered by other parts of the program,
   * such as in case of Species substitution in a Module
   * */
  
  public UnBindingReaction(Complex A, Complex B, Complex C){
    this.A = A;
    this.B = B;
    this.C = C;
  }
  
  public Set<Complex> getComplexes(){
    Set<Complex> rs = new HashSet<Complex>();
    rs.add(A);
    rs.add(B);
    rs.add(C);
    return rs;
  }
  
  /**
   * Constructor for cloning an UnBindingReaction object. This cloning
   * DEEP CLONES the Complex objects, but only copies the pointers
   * */
  
  public UnBindingReaction(UnBindingReaction er){
    this.A = new Complex(er.A);
    this.B = new Complex(er.B);
    this.C = new Complex(er.C);
  }
  
  @Override
  public Reaction deepCopy(){
    return new UnBindingReaction(this);
  }
  
  @Override
  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof UnBindingReaction))return false;
    UnBindingReaction er = (UnBindingReaction)other;  //downcast
    if (this.A.equals(er.A) && this.B.equals(er.B) && this.C.equals(er.C)) return true;
    return false;
  }
  
  @Override
  public int hashCode(){
    int hash = 17;
    hash += 19 * (null == A ? 0 : A.hashCode());
    hash += 23 * (null == B ? 0 : B.hashCode());
    hash += 29 * (null == C ? 0 : C.hashCode());
    return hash;
  } 
  
  public void setA(Complex A){
    this.A = A;
  }
  
  public Complex getA(){
    return this.A;
  }
  
  public void setB(Complex B){
    this.B = B;
  }
  
  public Complex getB(){
    return this.B;
  }

  public void setC(Complex C){
    this.C = C;
  }
  
  public Complex getC(){
    return this.C;
  }
  
  @Override
  public void substituteMember(Species oldS, Species newS){
    A.substituteMember(oldS,newS);
    B.substituteMember(oldS,newS);
    C.substituteMember(oldS,newS);
  }

  @Override
  public String toString(){
    return this.getA().toString() +
           " --> " + this.getB().toString() +
           " + " + this.getC().toString();
  }
  
  @Override
  public String printToSBML(int reactionIndex){
    String returnString = "";
    returnString += "      <reaction id=\"R" + reactionIndex + "\" reversible=\"false\" fast=\"false\"  >\n";
    returnString += "        <listOfReactants>\n";
    returnString += "          <speciesReference species=\"" + this.getA().toStringIDformat() + "\" stoichiometry=\"1\" />\n"; //stoichiometry is constant
    returnString += "        </listOfReactants>\n";
    returnString += "        <listOfProducts>\n";
    if(this.getB().equals(this.getC())){
      returnString += "          <speciesReference species=\"" + this.getB().toStringIDformat() + "\" stoichiometry=\"2\" />\n"; //stoichiometry is constant
    } else {
      returnString += "          <speciesReference species=\"" + this.getB().toStringIDformat() + "\" stoichiometry=\"1\" />\n"; //stoichiometry is constant
      returnString += "          <speciesReference species=\"" + this.getC().toStringIDformat() + "\" stoichiometry=\"1\" />\n"; //stoichiometry is constant
    }
    returnString += "        </listOfProducts>\n";
    returnString += "        <kineticLaw>\n";
    returnString += "          <math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n";
    returnString += "            <apply><times/><cn>1</cn><ci>" + this.getA().toStringIDformat() + "</ci></apply>\n";
    returnString += "          </math>\n";
    returnString += "        </kineticLaw>\n";
    returnString += "      </reaction>\n";
    return returnString;
  }

  @Override
  public String printToDOTv2(DotReactionClasses drc, int reactionIndex){
    String returnString = "";
    returnString += "\"" + this.getA().toString() + "\" -> R" + reactionIndex + "[arrowhead=None,weight=10,color=red];\n";
    returnString += "R" + reactionIndex + "[label=\"\", fixedsize=\"false\", width=0, height=0, shape=none];\n";
    returnString += "R" + reactionIndex + " -> \"" + this.getB().toString() + "\"[weight=10,color=red];\n";
    returnString += "R" + reactionIndex + " -> \"" + this.getC().toString() + "\"[weight=10,color=red];\n";
    //~ returnString += "\"" + this.getEnzyme().toString() + "\" -> R" + reactionIndex + "[style=dotted,arrowhead=odot];\n";
    //~ returnString += "\"" + er.getSubstrate().toString() + "\" -> \"" + er.getProduct().toString() + "\" [style=invis];\n";
    
    //~ returnString += "{ rank = same; \"" + er.getSubstrate().toString() + "\"; \"" + er.getProduct().toString() + "\"};\n";

    drc.add(this.getA(),this.getB(),"R" + reactionIndex);
    drc.add(this.getA(),this.getC(),"R" + reactionIndex);
    return returnString;
  }
  
}
