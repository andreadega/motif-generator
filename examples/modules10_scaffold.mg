
module
S1() -[ K1() ]-> pS1(x1,x2)
pS1(x1,x2) -[ P1() ]-> S1()
endmodule

module
S2(x3) -[ K2(x2) ]-> pS2(x4)
pS2(x4) -[ P2() ]-> S2(x3)
endmodule

module
M() + C1(x1) -> M()C1(x1)
M()C1(x1) -> M() + C1(x1) 
M() + C2(x3) -> M()C2(x3)
M()C2(x3) -> M() + C2(x3) 
M()C1(x1) + C2(x3) -> M()C1(x1)C2(x3)
M()C1(x1)C2(x3) -> M()C1(x1) + C2(x3)
M()C2(x3) + C1(x1) -> M()C1(x1)C2(x3)
M()C1(x1)C2(x3) -> M()C2(x3) + C1(x1)
M()C1(x1)C2(x3) -> M()C1(x1)C3(x4)
M()C1(x1)C3(x4) -> M()C1(x1) + C3(x4)
endmodule


strategy multimerge
