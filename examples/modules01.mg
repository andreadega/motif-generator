
module
A(ik) -[ KA(ak) ]-> pA(ak)
pA(ak) -[ PA(ap) ]-> A(ik)
endmodule

module
D(ik2) -[ KD(ak) ]-> pD(ak2)
pD(ak2) -[ PD(ap) ]-> D(ik2)
endmodule

strategy merge
