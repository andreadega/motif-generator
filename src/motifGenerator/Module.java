
package motifGenerator;

import java.util.*;

public class Module {
  
  private Set<Species> speciesSet;
  private Set<Reaction> reactionsSet;
  
  private Complex input;
  private Complex output;
  
  private Map<Complex,Double> initialConcentrations = new HashMap<Complex,Double>();
  
  /**
   * Constructor (set input and output variables to null and initialise 
   * a new empty Map<Complex,Double> to store initial concentrations)
   * */
  
  public Module(Set<Species> speciesSet,
            Set<Reaction> reactionsSet){
    this.speciesSet = speciesSet;
    this.reactionsSet = reactionsSet;
    this.input = null;
    this.output = null;
  }
  
  /**
  * Copy Constructor (Copies also pointers to input and output complexes,
  * instantiates new speciesSet and reactionsSet and intialConcentrations map
  * without deep copy of the content, just a copy of the pointers)
  * */
  
  public Module(Module em){
    this.speciesSet = new HashSet<Species>();
    this.speciesSet.addAll(em.getSpeciesSet());
    this.reactionsSet = new HashSet<Reaction>();
    this.reactionsSet.addAll(em.getReactionsSet());
    this.input = em.input;
    this.output = em.output;
    this.initialConcentrations.putAll(em.initialConcentrations);
  }
  
  /**
  * Constructor that merges two Module (set input and output variables to null and initialise 
   * a new empty Map<Complex,Double> to store initial concentrations)
  * */
  
  public Module(Module em1,Module em2){
    this.speciesSet = new HashSet<Species>();
    this.speciesSet.addAll(em1.getSpeciesSet());
    this.speciesSet.addAll(em2.getSpeciesSet());
    this.reactionsSet = new HashSet<Reaction>();
    this.reactionsSet.addAll(em1.getReactionsSet());
    this.reactionsSet.addAll(em2.getReactionsSet());
    this.input = null;
    this.output = null;    
  }
  
  public double getInitialConc(Complex c){
    double initialConc = 0.0;
    if (initialConcentrations.containsKey(c)){
      initialConc = (initialConcentrations.get(c)).doubleValue();
    }
    return initialConc;
  }
  
  public double getInitialConc(Species s){
    double initialConc = 0.0;
    Complex c = new Complex();
    c.add(s);
    if (initialConcentrations.containsKey(c)){
      initialConc = (initialConcentrations.get(c)).doubleValue();
    }
    return initialConc;
  }
  
  public Map<Complex,Double> getInitialConcMap(){
    return initialConcentrations;
  }
  
  public void setInitialConc(Complex c, double d){
    //to put all the initial concentrations in each module is highly inefficient...
    //but it is ok for now as a hack
    initialConcentrations.put(c,new Double(d));
  }
  
  /**
   * putAll copies all given mappings into the intialConcentration maps
   * of this module
   * */
  
  public void putAllInitialConc(Map<Complex,Double> icm){
    initialConcentrations.putAll(icm);
  }
  
  public void removeInitialConc(Complex c){
    initialConcentrations.remove(c);
  }
  
  public Set<Species> getSpeciesSet(){
    return this.speciesSet;
  }
  
  public Set<Complex> getComplexes(){
    Set<Complex> sc = new HashSet<Complex>();
    
    Iterator<Reaction> iteRe = this.getReactionsSet().iterator();
    while(iteRe.hasNext()){
      Reaction er = iteRe.next();
      sc.addAll(er.getComplexes());
    }
    return sc;
  }
  
  public Set<Reaction> getReactionsSet(){
    return this.reactionsSet;
  }
  
  public void setInput(Complex input){
    this.input = input;
  }

  public void setOutput(Complex output){
    this.output = output;
  }
  
  public Complex getInput(){
    return input;
  }

  public Complex getOutput(){
    return output;
  }
  
  public String toString(){
    String returnString = "List of Reactions for this module:\n";
    Iterator<Reaction> ite = reactionsSet.iterator();
    while (ite.hasNext()){
      Reaction er = ite.next();
      returnString += er.toString() + "\n";
    }
    return returnString;
  }
  
  /**
   * returns true if this and m share at least one species
   * */
  
  public boolean sharesSpeciesWith(Module m){
    Set<Species> s1 = this.getSpeciesSet();
    Set<Species> s2 = m.getSpeciesSet();
    
    if (s1.isEmpty() || s2.isEmpty()) return false;
    Iterator<Species> ite = s1.iterator();
    while (ite.hasNext()){
      Species tmp = ite.next();
      if (s2.contains(tmp)) return true;
    }
    return false;
    
  }
  
  //~ /**
   //~ * Print the module into a DOT format where species are on top, 
   //~ * visualised as circles and reactions are at the bottom as squares
   //~ * */
  //~ 
  //~ public String printToDOTv1(){
    //~ String returnString = "digraph G {\n";
    //~ int reactionIndex = 0;
    //~ Iterator<Reaction> iteRe = this.getreactionsSet().iterator();
    //~ while(iteRe.hasNext()){
      //~ Reaction er = iteRe.next();
      //~ reactionIndex++;
      //~ 
      //~ returnString += "\"" + er.getSubstrate().toString() + "\" -> R" + reactionIndex + " -> \"" + er.getProduct().toString() + "\";\n";
      //~ returnString += "\"" + er.getEnzyme().toString() + "\" -> R" + reactionIndex + "[style=dotted,arrowhead=odot];\n";
      //~ returnString += "R" + reactionIndex + " [shape=box,fontsize=8,height=0.2,width=0.3];\n";
      //~ 
    //~ }
    //~ 
    //~ returnString += "{ rank = same; ";
    //~ Iterator<Species> iteS = this.getSpeciesSet().iterator();
    //~ while(iteS.hasNext()){
      //~ Species S = iteS.next();
      //~ returnString += "\"" + S.toString() + "\"; ";
    //~ }
    //~ returnString += "}\n";
    //~ returnString += "}\n";
    //~ 
    //~ return returnString;
  //~ }
  
  /**
   * Print the module into a DOT format where enzymatic connections are 
   * represented by links from nodes to the middle of another arrow.
   * Middle points of arrows are hidden nodes
   * */
  
  public String printToDOTv2(){
    
    DotReactionClasses drc = new DotReactionClasses();
    
    String returnString = "digraph G {\nrankdir=LR;\n";
    int reactionIndex = 0;
    Iterator<Reaction> iteRe = this.getReactionsSet().iterator();
    while(iteRe.hasNext()){
      Reaction er = iteRe.next();
      reactionIndex++;
      returnString += er.printToDOTv2(drc,reactionIndex);
    }
    
    returnString += drc.printDOT_ReactionsInSameRank();
    
    //~ returnString += "{ rank = same; ";
    //~ Iterator<Species> iteS = this.getSpeciesSet().iterator();
    //~ while(iteS.hasNext()){
      //~ Species S = iteS.next();
      //~ returnString += S.toString() + "; ";
    //~ }
    //~ returnString += "}\n";
    returnString += "}\n";
    
    return returnString;
  }
  
  //COMMENTED BELOW BECAUSE BINDING/UNBINDING REQUIRE HYPEREDGES
  //~ /**
   //~ * Print the module into a DOT compact format
   //~ * */
  //~ 
  //~ public String printToDOTv3(){
    //~ String returnString = "digraph G {\n";
    //~ 
    //~ Iterator<Reaction> iteRe = this.getreactionsSet().iterator();
    //~ while(iteRe.hasNext()){
      //~ Reaction er = iteRe.next();      
      //~ returnString += "\"" + er.getSubstrate().toString() + "\" -> \"" + er.getProduct().toString() + "\" [label=\"" + er.getEnzyme().toString() + "\"];\n";
      //~ 
    //~ }
    //~ 
    //~ // returnString += "{ rank = same; ";
    //~ // Iterator<Species> iteS = this.getSpeciesSet().iterator();
    //~ // while(iteS.hasNext()){
      //~ // Species S = iteS.next();
      //~ // returnString += S.toString() + "; ";
    //~ // }
    //~ // returnString += "}\n";
    //~ returnString += "}\n";
    //~ 
    //~ return returnString;
  //~ }  
  
  /**
   * Print the module into SBML format
   * */

  public String printToSBML(){
    String returnString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<sbml xmlns=\"http://www.sbml.org/sbml/level2/version4\" level=\"2\" version=\"4\">\n"
            + "  <model id=\"Model\">\n";
    
    //compartments
    returnString += "    <listOfCompartments>\n";
    returnString += "      <compartment id=\"cyt\" constant=\"true\" size=\"1.0\" />\n";
    returnString += "    </listOfCompartments>\n";
    
    //species        
    returnString += "    <listOfSpecies>\n";
    // one or more <species> ... </species> elements
    Iterator<Complex> iteS = this.getComplexes().iterator();
    while(iteS.hasNext()){
      Complex S = iteS.next();
      returnString += "      <species id=\"" + S.toStringIDformat() + "\" initialAmount=\"" + this.getInitialConc(S) + "\" compartment=\"cyt\" constant=\"false\" boundaryCondition=\"false\" hasOnlySubstanceUnits=\"false\" />\n";
    }
    returnString += "    </listOfSpecies>\n";
    
    //reactions
    returnString += "    <listOfReactions>\n";
    // one or more <reaction> ... </reaction> elements
    Iterator<Reaction> iteRe = this.getReactionsSet().iterator();
    int reactionIndex = 0;
    while(iteRe.hasNext()){
      Reaction er = iteRe.next();      
      reactionIndex++;
      returnString += er.printToSBML(reactionIndex);
    }
    returnString += "    </listOfReactions>\n";    

    returnString += "  </model>\n";
    returnString += "</sbml>\n";
    
    return returnString;
  }  

}
