
module
S1(x1) -[ K1() ]-> pS1(x1)
pS1(x1) -[ P1() ]-> S1(x1)
S2(x3,x4) -[ pS1(x1) ]-> pS2(x4,x3)
pS2(x4,x3) -[ P2() ]-> S2(x3,x4)
endmodule

module
M() + C1(x1) -> M()C1(x1)
M()C1(x1) -> M() + C1(x1) 
M() + C2(x3) -> M()C2(x3)
M()C2(x3) -> M() + C2(x3) 
M()C1(x1) + C2(x3) -> M()C1(x1)C2(x3)
M()C1(x1)C2(x3) -> M()C1(x1) + C2(x3)
M()C2(x3) + C1(x1) -> M()C1(x1)C2(x3)
M()C1(x1)C2(x3) -> M()C2(x3) + C1(x1)
M()C1(x1)C2(x3) -> M()C1(x1)C3(x4)
M()C1(x1)C3(x4) -> M()C1(x1) + C3(x4)
endmodule


strategy multimerge
