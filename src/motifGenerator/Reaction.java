
package motifGenerator;

import java.util.*;

public abstract class Reaction {
  
  public abstract void substituteMember(Species oldS, Species newS);
  public abstract Reaction deepCopy();
  public abstract boolean equals(Object other);
  public abstract int hashCode();
  public abstract String toString();
  public abstract Set<Complex> getComplexes();
  public abstract String printToSBML(int reactionIndex);
  public abstract String printToDOTv2(DotReactionClasses drc, int reactionIndex);
}
