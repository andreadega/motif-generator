
package motifGenerator;

import java.util.*;

public class Species {
  
  public static final int KINASE = 0;
  public static final int PHOSPHATASE = 1;
  public static final int REGULATOR = 2;  
  
  private String name;
  private Set<String> setOfProperties;
  
  /**
   * Constructor for a new Species object
   * */
  
  public Species(String name, Set<String> setOfProperties){
    this.name = name;
    this.setOfProperties = setOfProperties;
  }
  
  /**
   * Constructor for cloning a Species object
   * */
  
  public Species(Species S){
    this.name = S.name;
    this.setOfProperties = S.setOfProperties;
  }
  
  /**
   * Overwrite equals() Object method
   * */
   
  @Override
  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Species))return false;
    Species s = (Species)other;  //downcast
    if (this.name.equals(s.name) && this.setOfProperties.equals(s.setOfProperties)) return true;
    return false;
  }
  
  @Override
  public int hashCode(){
    int hash = 7;
    hash += 13 * (null == name ? 0 : name.hashCode());
    hash += 11 * (null == setOfProperties ? 0 : setOfProperties.hashCode());
    return hash;
  }  
  
  public String getName(){
    return this.name;
  }
  
  public Set<String> getSetOfProperties(){
    return this.setOfProperties;
  }
  
  public String toStringProperties(){
    
    String returnString = "";
    
    Iterator<String> ite = setOfProperties.iterator();
    
    if (ite.hasNext()) returnString += ite.next();
    
    while(ite.hasNext()){
      returnString += "," + ite.next();
    }
    
    return returnString;
  }
  
  public String toStringPropertiesIDformat(){
    
    String returnString = "";
    
    Iterator<String> ite = setOfProperties.iterator();
    
    if (ite.hasNext()) returnString += ite.next();
    
    while(ite.hasNext()){
      returnString += "_" + ite.next();
    }
    
    return returnString;
  }
  
  public String toString(){
    return name + "(" + toStringProperties() + ")";
  }

  public String toStringIDformat(){
    return name + "_" + toStringPropertiesIDformat();
  }  
  
  
  /**
   * Return true if the other Species shares at least one property
   * */
  
  public boolean hasSameRoleOf(Species other){
    if (other == null) return false;
    if (other == this) return true;
    if (intersects(this.setOfProperties,other.setOfProperties)) return true;
    return false;
  }
  
  /**
   * check for intersection of two String sets
   * */

  private boolean intersects(Set<String> s1, Set<String> s2){
    if (s1.isEmpty() || s2.isEmpty()) return false;
    Iterator<String> ite = s1.iterator();
    while (ite.hasNext()){
      String tmp = ite.next();
      if (s2.contains(tmp)) return true;
    }
    return false;
  }

}

