
package motifGenerator;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import antlr.*;
import java.util.*;
import java.text.*;
import java.io.*;
import motifGenerator.*;
import javax.swing.JFileChooser; 

public class Main {
  public static void main(String[] args) throws Exception {
    
    
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    //get current date time with Date()
    Date date = new Date();
    //System.out.println(dateFormat.format(date));

    //get current date time with Calendar()
    Calendar cal = Calendar.getInstance();
    //System.out.println(dateFormat.format(cal.getTime()));
        
    String inputFileName = "modules.txt";
    BufferedWriter bw = null; 
      
    try{
      
      File outputFile = new File("results.log");        
      if (!outputFile.exists()) {
        outputFile.createNewFile();
      }
      FileWriter fw = new FileWriter(outputFile.getAbsoluteFile()); 
      bw = new BufferedWriter(fw);
      
      //bw.write("[info] number of arguments: " + args.length + "\n");
      bw.write("[info] start program at: " + dateFormat.format(cal.getTime()) + "\n");
      
      if (args.length == 0){
        // Add something here as graphic input?
        
        JFileChooser chooser = new JFileChooser(new File("."));
        int returnVal = chooser.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          inputFileName = chooser.getSelectedFile().getAbsolutePath();
          bw.write("[info] file selected from input file dialog: "+ inputFileName +"\n");
        } else {
          bw.write("[info] input file dialog cancelled by user.\n");
          File f = new File("modules.txt");
          if (f.exists()){
            bw.write("[info] no input file specified, using \"modules.txt\".\n");
          }else{
            bw.write("[error] no input file specified, trying to use \"modules.txt\", but file not found!\n");
            //bw.close(); //finally below takes care of this
            return;
          }  
        }
        
      } else if (args.length == 1){
        inputFileName = args[0];
        File f = new File(inputFileName);
        if (f.exists()){
          bw.write("[info] input file given by user, using \""+ inputFileName +"\".\n");
        }else{
          bw.write("[error] input file given by user, namely \""+ inputFileName +"\", but file not found!\n");
          //bw.close(); //finally below takes care of this
          return;
        }
      } else {
        
        bw.write("[error] wrong number of arguments specified! Either run with no arguments (module.txt as input file) or with input file as argument.\n");
        //bw.close(); //finally below takes care of this
        return;
      }
        
        
      motifGeneratorLexer lexer = new motifGeneratorLexer(new ANTLRFileStream(inputFileName));
      motifGeneratorParser parser = new motifGeneratorParser(new CommonTokenStream(lexer));
      
      parser.removeErrorListeners(); // remove ConsoleErrorListener
      parser.addErrorListener(new OutputFileListener(bw)); // add ours
      
      ParseTree tree = parser.code();
      Options opts = new Options();
      MGVisitor visitor = new MGVisitor(opts);
      
      visitor.visit(tree);
      
      //retrieve main module list (need to merge all together)
      LinkedList<Module> mainModuleList = opts.getMainModuleList();
      //retrieve groups of modules (need to merge only one per group)
      HashMap<String,LinkedList<Module>> groupsOfModules = opts.getGroups();
      
      //lists of modules to merge
      LinkedList<LinkedList<Module>> listsOfModules = null;
      
      // Before I use the merge algorithms, I need to:
      // 1. get all Module combinations from the groups of Modules. This
      //    yields one list of module for each combination of one Module
      //    per group
      // 2. add to each list the main module list
      
      if (mainModuleList.isEmpty() && groupsOfModules.isEmpty()) {
        bw.write("[error] no modules given!\n");
        //bw.close(); //finally below takes care of this
        return;
      } else if (mainModuleList.isEmpty()){
        //if I'm here then only groupsOfModules is not empty
        listsOfModules = getAllCombinationsOfGroupModules(groupsOfModules);
      } else if (groupsOfModules.isEmpty()){
        //if I'm here then only mainModuleList is not empty
        listsOfModules = new LinkedList<LinkedList<Module>>();
        listsOfModules.add(mainModuleList);
      } else {
        //if I'm here both mainModuleList and groupsOfModules are not empty
        //add the main Module list to each list
        listsOfModules = getAllCombinationsOfGroupModules(groupsOfModules);
        Iterator<LinkedList<Module>> iteList = listsOfModules.iterator();
        while (iteList.hasNext()){
          LinkedList<Module> tmp = iteList.next();
          tmp.addAll(mainModuleList);
        }
        
      }
      
      //---------------------------------------
      
      
      //Need to iterate across module lists
      
      //---------------------------------------
      
      //check that there are no overlapping species in the modules
      

      Iterator<LinkedList<Module>> iteML = listsOfModules.iterator();
      while (iteML.hasNext()){
        LinkedList<Module> moduleList = iteML.next();
        if (MotifGenerator.modulesShareSpecies(moduleList)){
          bw.write("[error] modules share species! Two modules cannot contain the same species unless they are in the same group (only one module will be use for each group).\n");
          //bw.close(); //finally below takes care of this
          return;
        }
      }        
    
      // add initial concentrations of main module list
      Iterator<Module> ite = mainModuleList.iterator();
      while (ite.hasNext()){
        Module tmp = ite.next();
        tmp.putAllInitialConc(opts.getInitialConcMap());
      }
      
      // add initial concentrations of modules in groups
      Iterator<String> iteG = groupsOfModules.keySet().iterator();
      while (iteG.hasNext()){
        String tmp = iteG.next();
        //retrieve list for this group and iterate through modules in the list
        ite = groupsOfModules.get(tmp).iterator();
        while (ite.hasNext()){
          Module tmp2 = ite.next();
          tmp2.putAllInitialConc(opts.getInitialConcMap());
        }        
      }      

      // print initial modules of main module list
      PrintTool.printToDOT(mainModuleList,2,"initialModule");
      PrintTool.printToSBML(mainModuleList,"initialModule");

      // print initial modules of modules in groups
      iteG = groupsOfModules.keySet().iterator();
      while (iteG.hasNext()){
        String tmp = iteG.next();
        PrintTool.printToDOT(groupsOfModules.get(tmp),2,"initialModule_group" + tmp);
        PrintTool.printToSBML(groupsOfModules.get(tmp),"initialModule_group" + tmp);      
      }        

      
      //////////////////////
      // At this point opts contains the initial concentrations for
      // the complexes (opts.getInitialConcMap()) and the overall I/O
      // after merging. If these were not specified the I/O are null and
      // the init is an empty Map
        
      if (opts.getStrategy() != null) bw.write("[info] selected strategy: " + opts.getStrategy() + "\n");

      LinkedList<Module> newModules = new LinkedList<Module>();
      
      if (opts.getStrategy().equals("merge") || opts.getStrategy().equals("multimerge")){
        iteML = listsOfModules.iterator();
        while (iteML.hasNext()){
          LinkedList<Module> moduleList = iteML.next();
          newModules.addAll(MotifGenerator.mergeModuleList(moduleList,opts));
        }  
      } else {
          bw.write("[error] no valid strategy given! Please specify a strategy (merge or multimerge) in the input file:\n");
          return;
      }
      
      if (newModules == null) {
          bw.write("[error] no motifs were generated!\n");
          //bw.close(); //finally below takes care of this
          return;
      }


              
      
      bw.write("[info] generated motifs (" + newModules.size() + "):\n");
      bw.write(newModules.toString() + "\n");
      
      PrintTool.printToDOT(newModules,2,"generatedMotif");
      PrintTool.printToSBML(newModules,"generatedMotif");
      
      //moved to finally
      //bw.flush();
      //bw.close();
    } catch (IOException e){
      bw.write("[error] IOException!\n");
      e.printStackTrace();
    } catch (NullPointerException npe){
      bw.write("[error] NullPointerException!\n");
      npe.printStackTrace();
    } catch (CannotMergeException cme){
      bw.write(cme.getMessage() + "\n");
    }finally { //this is always executed, even if a return is called
      bw.flush();
      bw.close();
    }
  }
  
  /**
   * Return null if groups is empty
   * */
  private static LinkedList<LinkedList<Module>> getAllCombinationsOfGroupModules(HashMap<String,LinkedList<Module>> groups){
    if (groups.isEmpty()) return null;
    LinkedList<LinkedList<Module>> returnList = new LinkedList<LinkedList<Module>>();
    
    int ngroups = groups.size();
    int[] groupSizes = new int[ngroups];
    int[] groupIndices = new int[ngroups];
    for (int i=0;i<ngroups;i++) groupIndices[i] = 0;
    
    //iterate through groups
    int i = 0;
    Iterator<String> ite = groups.keySet().iterator();
    while (ite.hasNext()){
      String groupID = ite.next();
      LinkedList<Module> group = groups.get(groupID);
      groupSizes[i] = group.size();
      i++;
    }
    
    //get the lists
    while(groupIndices[ngroups-1]<groupSizes[ngroups-1]){ //until we have done all combinations
      LinkedList<Module> newList = new LinkedList<Module>();
      
      //iterate through groups and get the current combination (groupIndices)
      i = 0;
      Iterator<String> ite2 = groups.keySet().iterator();
      while (ite2.hasNext()){
        String groupID = ite2.next();
        LinkedList<Module> group = groups.get(groupID);
        newList.add(group.get(groupIndices[i]));
        i++;
      }
      
      returnList.add(newList);
      
      //update indices
      groupIndices[0]++; //increase the first index
      for (i=0;i<ngroups-1;i++) {
        if (groupIndices[i] >= groupSizes[i]){
          //if the index i goes out of bound of array, reset it to zero 
          //and increase next index
          groupIndices[i] = 0;
          groupIndices[i+1]++;
        }
      }
    }
    return returnList;
  }
}
