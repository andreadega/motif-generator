
package motifGenerator;

import java.util.*;
import java.io.*;

public class PrintTool {

  /**
   * export motifs to dot files
   * */
  
  public static void printToDOT(LinkedList<Module> motifs, int version, String tag){
    
    int i = 0;
    Iterator<Module> iteEM = motifs.iterator();
    
    while (iteEM.hasNext()){
      //call next() before i/o operations
      Module tmpEM = iteEM.next();
      
      try{
        File outputFile = new File("motifs/" + tag + "_motif" + (++i) + "_ver" + version + ".gv");
        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        
        //Write file
        if (version == 1) {
          //~ bw.write(tmpEM.printToDOTv1());
        } else if (version == 2) {
          bw.write(tmpEM.printToDOTv2());
        } else if (version == 3) {
          //~ bw.write(tmpEM.printToDOTv3());
        } else {
          bw.write("Invalid format specified\n");
        }
        //Done writing module
        
        bw.close();
      }catch(IOException e){
        e.printStackTrace();
      }
    }
    
  }
  
  
  /**
   * export motifs to sbml files
   * */
  
  public static void printToSBML(LinkedList<Module> motifs, String tag){
    
    int i = 0;
    Iterator<Module> iteEM = motifs.iterator();
    
    while (iteEM.hasNext()){
      //call next() before i/o operations
      Module tmpEM = iteEM.next();
      
      try{
        File outputFile = new File("motifs/"+ tag + "_motif" + (++i) + ".sbml");
        
        if (!outputFile.exists()) {
          outputFile.createNewFile();
        }
        
        FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        
        //Write file

          bw.write(tmpEM.printToSBML());

        //Done writing module
        
        bw.close();
      }catch(IOException e){
        e.printStackTrace();
      }
    }
    
  }
  
  //~ /**
   //~ * export motifs to Mathematica
   //~ * */
  //~ 
  //~ public static void printToMathematica(LinkedList<Module> motifs){
    //~ 
    //~ int i = 0;
    //~ Iterator<Module> iteEM = motifs.iterator();
      //~ 
    //~ try{
      //~ File outputFile = new File("motifs/Mathematica.txt");
      //~ 
      //~ if (!outputFile.exists()) {
        //~ outputFile.createNewFile();
      //~ }
      //~ 
      //~ FileWriter fw = new FileWriter(outputFile.getAbsoluteFile());
      //~ BufferedWriter bw = new BufferedWriter(fw);
      //~ 
      //~ //Write file
      //~ 
      //~ 
      //~ bw.write("{ \n");
        //~ 
      //~ while (iteEM.hasNext()){
        //~ Module tmpEM = iteEM.next();
        //~ 
        //~ bw.write("  { motif" + (++i));
        //~ 
        //~ int reactionIndex = 0;
        //~ Iterator<Reaction> iteRe = tmpEM.getReactionsSet().iterator();
        //~ while(iteRe.hasNext()){
          //~ Reaction er = iteRe.next();
          //~ reactionIndex++;
          //~ 
          //~ bw.write(",{" + er.getSubstrate().toString() + " -> " + er.getProduct().toString() + "," + er.getEnzyme().toString() + "}");
          //~ 
        //~ }
        //~ if (i!=motifs.size()){
          //~ bw.write("},\n");
        //~ } else {
          //~ bw.write("}\n");
        //~ }
      //~ }
      //~ bw.write("}\n");
      //~ //Done writing 
      //~ 
      //~ bw.close();
    //~ }catch(IOException e){
      //~ e.printStackTrace();
    //~ }
  //~ 
    //~ 
  //~ }

}
