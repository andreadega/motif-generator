// Generated from motifGenerator.g4 by ANTLR 4.1
package antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link motifGeneratorParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface motifGeneratorVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#moduleRuleIO}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModuleRuleIO(@NotNull motifGeneratorParser.ModuleRuleIOContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#moduleLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModuleLine(@NotNull motifGeneratorParser.ModuleLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#moduleRuleGroup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModuleRuleGroup(@NotNull motifGeneratorParser.ModuleRuleGroupContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#fuseToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuseToken(@NotNull motifGeneratorParser.FuseTokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#mergeTokenIO}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMergeTokenIO(@NotNull motifGeneratorParser.MergeTokenIOContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#speciesRuleEmptyProperties}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpeciesRuleEmptyProperties(@NotNull motifGeneratorParser.SpeciesRuleEmptyPropertiesContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#mergeToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMergeToken(@NotNull motifGeneratorParser.MergeTokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#idListList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdListList(@NotNull motifGeneratorParser.IdListListContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#skipstuff}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSkipstuff(@NotNull motifGeneratorParser.SkipstuffContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#initLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitLine(@NotNull motifGeneratorParser.InitLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#multimergeToken}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultimergeToken(@NotNull motifGeneratorParser.MultimergeTokenContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#unbindingReaction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnbindingReaction(@NotNull motifGeneratorParser.UnbindingReactionContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#enzymaticReaction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnzymaticReaction(@NotNull motifGeneratorParser.EnzymaticReactionContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#codeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCodeList(@NotNull motifGeneratorParser.CodeListContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#initDOUBLE}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitDOUBLE(@NotNull motifGeneratorParser.InitDOUBLEContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#initINT}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitINT(@NotNull motifGeneratorParser.InitINTContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#speciesRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpeciesRule(@NotNull motifGeneratorParser.SpeciesRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#codeRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCodeRule(@NotNull motifGeneratorParser.CodeRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#bindingReaction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBindingReaction(@NotNull motifGeneratorParser.BindingReactionContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#reactionRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReactionRule(@NotNull motifGeneratorParser.ReactionRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#idListTerm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdListTerm(@NotNull motifGeneratorParser.IdListTermContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#complexRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplexRule(@NotNull motifGeneratorParser.ComplexRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#strategyLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrategyLine(@NotNull motifGeneratorParser.StrategyLineContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#moduleRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModuleRule(@NotNull motifGeneratorParser.ModuleRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#reactionList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReactionList(@NotNull motifGeneratorParser.ReactionListContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#conversionReaction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConversionReaction(@NotNull motifGeneratorParser.ConversionReactionContext ctx);

	/**
	 * Visit a parse tree produced by {@link motifGeneratorParser#complexList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplexList(@NotNull motifGeneratorParser.ComplexListContext ctx);
}