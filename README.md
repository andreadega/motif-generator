
## Signalling Pathway Motifs Generator

    Andrea Degasperi
    Systems Biology Ireland,
    Conway Institute,
    University College Dublin,
    Dublin, Republic of Ireland

As part of the Synthetic Cellular Signalling Circuits (SynSignal) FP7
European Project, we want to develop a software for the rational design
of synthetic signalling pathways. These are constituted of proteins that
can bind-unbind and undergo post-translational modifications such as
phosphorylation.
The software should be able to combine small building blocks circuits of
well characterised behaviour such that a desired input-output or
temporal dynamics is obtained.
With this program we begin addressing the theoretical question of
combining small circuits (modules) to generate signalling motifs
automatically. We provide an input language where modules can be specified
as set of reactions. The syntax allows for the definition of molecular
properties which can express function or activity (e.g. kinase, phosphatase,
active...) or can be used just as labels which in turn can be used by 
merging strategies. Merging strategies define how to combine modules and
generate complex circuits.
The next step is to add the possibility of specifying a quantitative
behaviour of the circuits and evaluate the behaviour of the generated
motifs.

To use the program download a zipped build from the 'download' page of the
bitbucket website (https://bitbucket.org/andreadega/motif-generator/downloads).
A manual can be found in the ```docs``` directory or in the bitbucket
wiki page (https://bitbucket.org/andreadega/motif-generator/wiki/Home). 

### Versions:

- Version 1.1:
    - Added multimerge. Two modules can merge as many compatible
      species as possible at once.
    - Added groups. Modules in the same group are mutually exclusive,
      i.e. only one module from each group is used to create models.

- Version 1.051:
    - fixed a bug in SBML initial concentration.
    - fixed a NullPointerException when I/O is not specified.

- Version 1.05:
    - added the possibility to indicate input/output of modules and 
      merged modules (not yet used).
    - added the possibility to indicate initial concentration of species
      or complexes (default is 0.0).
    - SBML models can now be executed if imported in softwares like Copasi.
      For the moment, it is not possible to specify reaction rates, which
      are assumed to be mass action with kinetic constant k=1, with the 
      exception of the enzymatic reaction that uses michaelis-menten with 
      Vmax=1 and Km=0.5.

- Version 1.01:
    - fixed a bug in the generation of SBML.

- Version 1:
    - when you run the jar you can select an input file. Pressing cancel
      will try to load 'modules.txt'.
    - information such as generated motifs, progress of the execution
      and errors are given in a results.log file.
    - only one merge strategy (called 'merge'). Two species are merged
      if they share a property and they are both substituted by a new
      species (newly generated name) that shares properties of both.
    - Importantly, we have now the restriction that the defined modules
      cannot have species in common.


### Acknowledgements: 

The research leading to these results has received funding from the 
European Union Seventh Framework Programme (FP7/2007-2013) under grant
agreement no 613879.
