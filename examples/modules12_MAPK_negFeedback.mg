
module
Raf(x) -[ K1() ]-> ActRaf(x)
ActRaf(x) -[ P1(x) ]-> Raf(x)
MEK(x) -[ ActRaf(x) ]-> ppMEK(x)
ppMEK(x) -[ P2(x) ]-> MEK(x)
ERK() -[ ppMEK(x) ]-> ppERK(y)
ppERK(y) -[ P3(x) ]-> ERK()
endmodule

module
A(x) + B(y) -> A(x)B(y)
A(x)B(y) -> A(x) + B(y)
endmodule


strategy multimerge
