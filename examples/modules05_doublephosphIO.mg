
module: input KA(), output pA(kinase)
A() -[ KA() ]-> pA(kinase)
pA(kinase) -[ PA() ]-> A()
endmodule

module: input KD(kinase), output pD()
D() -[ KD(kinase) ]-> pD()
pD() -[ PD() ]-> D()
endmodule

init KA() 1
init A() 1
init PA() 1
init KD(kinase) 1
init D() 1
init PD() 1

strategy merge: input KA(), output pD()
