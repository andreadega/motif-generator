
module
A(d) -[ KA(c,e) ]-> pA(b,pd,f)
pA(b,pd,f) -[ PA(c,e) ]-> A(d)
endmodule

module group feedback
B(b) + C(c) -> B(b)C(c)
B(b)C(c) -> B(b) + C(c)
endmodule

module group feedback
D(d) -[ pD(pd) ]-> pD(pd)
endmodule

module group feedback
E(e) -[ pF(f) ]-> E(e)pF(f)
endmodule

strategy multimerge
