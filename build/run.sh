#!/bin/bash

#cd src/antlr
#antlr4 -no-listener -visitor -package antlr motifGenerator.g4
#cd ../..

#javac -Xlint -d classes -cp lib/antlr-4.1-complete.jar src/motifGenerator/*.java src/com/scottlogic/util/SortedList.java src/antlr/*.java 

#jar cfm motifGenerator.jar Manifest.txt -C classes/ .

#java -cp classes:lib/antlr-4.1-complete.jar motifGenerator.Main $1


./cleanMotifs.sh

java -jar motifGenerator.jar $1

cat results.log

cd  motifs
./dotAll.sh
