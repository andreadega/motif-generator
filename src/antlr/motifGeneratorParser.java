// Generated from motifGenerator.g4 by ANTLR 4.1
package antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class motifGeneratorParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__10=1, T__9=2, T__8=3, T__7=4, T__6=5, T__5=6, T__4=7, T__3=8, T__2=9, 
		T__1=10, T__0=11, INT=12, DOUBLE=13, NEWLINE=14, SPACETABS=15, MODULESTART=16, 
		MODULEEND=17, STRATEGY=18, MERGE=19, MULTIMERGE=20, FUSE=21, INIT=22, 
		ID=23;
	public static final String[] tokenNames = {
		"<INVALID>", "']->'", "'->'", "'input'", "')'", "','", "'output'", "'+'", 
		"':'", "'('", "'group'", "'-['", "INT", "DOUBLE", "NEWLINE", "SPACETABS", 
		"'module'", "'endmodule'", "'strategy'", "'merge'", "'multimerge'", "'fuse'", 
		"'init'", "ID"
	};
	public static final int
		RULE_code = 0, RULE_line = 1, RULE_module = 2, RULE_reactions = 3, RULE_reaction = 4, 
		RULE_complex = 5, RULE_species = 6, RULE_idList = 7, RULE_strategynt = 8, 
		RULE_init = 9;
	public static final String[] ruleNames = {
		"code", "line", "module", "reactions", "reaction", "complex", "species", 
		"idList", "strategynt", "init"
	};

	@Override
	public String getGrammarFileName() { return "motifGenerator.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public motifGeneratorParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class CodeContext extends ParserRuleContext {
		public CodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_code; }
	 
		public CodeContext() { }
		public void copyFrom(CodeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CodeRuleContext extends CodeContext {
		public TerminalNode EOF() { return getToken(motifGeneratorParser.EOF, 0); }
		public LineContext line() {
			return getRuleContext(LineContext.class,0);
		}
		public CodeRuleContext(CodeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitCodeRule(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CodeListContext extends CodeContext {
		public LineContext line() {
			return getRuleContext(LineContext.class,0);
		}
		public CodeContext code() {
			return getRuleContext(CodeContext.class,0);
		}
		public CodeListContext(CodeContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitCodeList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CodeContext code() throws RecognitionException {
		CodeContext _localctx = new CodeContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_code);
		try {
			setState(26);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				_localctx = new CodeRuleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(20); line();
				setState(21); match(EOF);
				}
				break;

			case 2:
				_localctx = new CodeListContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(23); line();
				setState(24); code();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineContext extends ParserRuleContext {
		public LineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line; }
	 
		public LineContext() { }
		public void copyFrom(LineContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ModuleLineContext extends LineContext {
		public TerminalNode NEWLINE() { return getToken(motifGeneratorParser.NEWLINE, 0); }
		public TerminalNode SPACETABS() { return getToken(motifGeneratorParser.SPACETABS, 0); }
		public ModuleContext module() {
			return getRuleContext(ModuleContext.class,0);
		}
		public ModuleLineContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitModuleLine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StrategyLineContext extends LineContext {
		public TerminalNode NEWLINE() { return getToken(motifGeneratorParser.NEWLINE, 0); }
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public StrategyntContext strategynt() {
			return getRuleContext(StrategyntContext.class,0);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public TerminalNode STRATEGY() { return getToken(motifGeneratorParser.STRATEGY, 0); }
		public StrategyLineContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitStrategyLine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SkipstuffContext extends LineContext {
		public TerminalNode NEWLINE() { return getToken(motifGeneratorParser.NEWLINE, 0); }
		public TerminalNode SPACETABS() { return getToken(motifGeneratorParser.SPACETABS, 0); }
		public SkipstuffContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitSkipstuff(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InitLineContext extends LineContext {
		public TerminalNode NEWLINE() { return getToken(motifGeneratorParser.NEWLINE, 0); }
		public TerminalNode SPACETABS() { return getToken(motifGeneratorParser.SPACETABS, 0); }
		public InitContext init() {
			return getRuleContext(InitContext.class,0);
		}
		public InitLineContext(LineContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitInitLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LineContext line() throws RecognitionException {
		LineContext _localctx = new LineContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_line);
		int _la;
		try {
			setState(52);
			switch (_input.LA(1)) {
			case NEWLINE:
			case SPACETABS:
				_localctx = new SkipstuffContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(29);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(28); match(SPACETABS);
					}
				}

				setState(31); match(NEWLINE);
				}
				break;
			case MODULESTART:
				_localctx = new ModuleLineContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(32); module();
				setState(34);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(33); match(SPACETABS);
					}
				}

				setState(36); match(NEWLINE);
				}
				break;
			case STRATEGY:
				_localctx = new StrategyLineContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(38); match(STRATEGY);
				setState(39); match(SPACETABS);
				setState(40); strategynt();
				setState(42);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(41); match(SPACETABS);
					}
				}

				setState(44); match(NEWLINE);
				}
				break;
			case INIT:
				_localctx = new InitLineContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(46); init();
				setState(48);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(47); match(SPACETABS);
					}
				}

				setState(50); match(NEWLINE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModuleContext extends ParserRuleContext {
		public ModuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_module; }
	 
		public ModuleContext() { }
		public void copyFrom(ModuleContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ModuleRuleIOContext extends ModuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(motifGeneratorParser.NEWLINE); }
		public ReactionsContext reactions() {
			return getRuleContext(ReactionsContext.class,0);
		}
		public TerminalNode MODULEEND() { return getToken(motifGeneratorParser.MODULEEND, 0); }
		public TerminalNode MODULESTART() { return getToken(motifGeneratorParser.MODULESTART, 0); }
		public TerminalNode NEWLINE(int i) {
			return getToken(motifGeneratorParser.NEWLINE, i);
		}
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public List<ComplexContext> complex() {
			return getRuleContexts(ComplexContext.class);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public ComplexContext complex(int i) {
			return getRuleContext(ComplexContext.class,i);
		}
		public ModuleRuleIOContext(ModuleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitModuleRuleIO(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ModuleRuleGroupContext extends ModuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(motifGeneratorParser.NEWLINE); }
		public ReactionsContext reactions() {
			return getRuleContext(ReactionsContext.class,0);
		}
		public TerminalNode MODULEEND() { return getToken(motifGeneratorParser.MODULEEND, 0); }
		public TerminalNode MODULESTART() { return getToken(motifGeneratorParser.MODULESTART, 0); }
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public TerminalNode NEWLINE(int i) {
			return getToken(motifGeneratorParser.NEWLINE, i);
		}
		public TerminalNode ID() { return getToken(motifGeneratorParser.ID, 0); }
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public ModuleRuleGroupContext(ModuleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitModuleRuleGroup(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ModuleRuleContext extends ModuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(motifGeneratorParser.NEWLINE); }
		public ReactionsContext reactions() {
			return getRuleContext(ReactionsContext.class,0);
		}
		public TerminalNode MODULEEND() { return getToken(motifGeneratorParser.MODULEEND, 0); }
		public TerminalNode MODULESTART() { return getToken(motifGeneratorParser.MODULESTART, 0); }
		public TerminalNode SPACETABS() { return getToken(motifGeneratorParser.SPACETABS, 0); }
		public TerminalNode NEWLINE(int i) {
			return getToken(motifGeneratorParser.NEWLINE, i);
		}
		public ModuleRuleContext(ModuleContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitModuleRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModuleContext module() throws RecognitionException {
		ModuleContext _localctx = new ModuleContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_module);
		int _la;
		try {
			setState(109);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				_localctx = new ModuleRuleIOContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(54); match(MODULESTART);
				setState(56);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(55); match(SPACETABS);
					}
				}

				setState(58); match(8);
				setState(60);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(59); match(SPACETABS);
					}
				}

				setState(62); match(3);
				setState(63); match(SPACETABS);
				setState(64); complex();
				setState(66);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(65); match(SPACETABS);
					}
				}

				setState(68); match(5);
				setState(70);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(69); match(SPACETABS);
					}
				}

				setState(72); match(6);
				setState(73); match(SPACETABS);
				setState(74); complex();
				setState(76);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(75); match(SPACETABS);
					}
				}

				setState(78); match(NEWLINE);
				setState(79); reactions();
				setState(80); match(NEWLINE);
				setState(81); match(MODULEEND);
				}
				break;

			case 2:
				_localctx = new ModuleRuleContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(83); match(MODULESTART);
				setState(85);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(84); match(SPACETABS);
					}
				}

				setState(87); match(NEWLINE);
				setState(88); reactions();
				setState(89); match(NEWLINE);
				setState(90); match(MODULEEND);
				}
				break;

			case 3:
				_localctx = new ModuleRuleGroupContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(92); match(MODULESTART);
				setState(94);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(93); match(SPACETABS);
					}
				}

				setState(96); match(10);
				setState(98);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(97); match(SPACETABS);
					}
				}

				setState(100); match(ID);
				setState(102);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(101); match(SPACETABS);
					}
				}

				setState(104); match(NEWLINE);
				setState(105); reactions();
				setState(106); match(NEWLINE);
				setState(107); match(MODULEEND);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReactionsContext extends ParserRuleContext {
		public ReactionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reactions; }
	 
		public ReactionsContext() { }
		public void copyFrom(ReactionsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ReactionRuleContext extends ReactionsContext {
		public ReactionContext reaction() {
			return getRuleContext(ReactionContext.class,0);
		}
		public ReactionRuleContext(ReactionsContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitReactionRule(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReactionListContext extends ReactionsContext {
		public TerminalNode NEWLINE() { return getToken(motifGeneratorParser.NEWLINE, 0); }
		public ReactionsContext reactions() {
			return getRuleContext(ReactionsContext.class,0);
		}
		public TerminalNode SPACETABS() { return getToken(motifGeneratorParser.SPACETABS, 0); }
		public ReactionContext reaction() {
			return getRuleContext(ReactionContext.class,0);
		}
		public ReactionListContext(ReactionsContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitReactionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReactionsContext reactions() throws RecognitionException {
		ReactionsContext _localctx = new ReactionsContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_reactions);
		int _la;
		try {
			setState(119);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				_localctx = new ReactionRuleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(111); reaction();
				}
				break;

			case 2:
				_localctx = new ReactionListContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(112); reaction();
				setState(114);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(113); match(SPACETABS);
					}
				}

				setState(116); match(NEWLINE);
				setState(117); reactions();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReactionContext extends ParserRuleContext {
		public ReactionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reaction; }
	 
		public ReactionContext() { }
		public void copyFrom(ReactionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BindingReactionContext extends ReactionContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public List<ComplexContext> complex() {
			return getRuleContexts(ComplexContext.class);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public ComplexContext complex(int i) {
			return getRuleContext(ComplexContext.class,i);
		}
		public BindingReactionContext(ReactionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitBindingReaction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnbindingReactionContext extends ReactionContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public List<ComplexContext> complex() {
			return getRuleContexts(ComplexContext.class);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public ComplexContext complex(int i) {
			return getRuleContext(ComplexContext.class,i);
		}
		public UnbindingReactionContext(ReactionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitUnbindingReaction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConversionReactionContext extends ReactionContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public List<ComplexContext> complex() {
			return getRuleContexts(ComplexContext.class);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public ComplexContext complex(int i) {
			return getRuleContext(ComplexContext.class,i);
		}
		public ConversionReactionContext(ReactionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitConversionReaction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EnzymaticReactionContext extends ReactionContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public List<ComplexContext> complex() {
			return getRuleContexts(ComplexContext.class);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public ComplexContext complex(int i) {
			return getRuleContext(ComplexContext.class,i);
		}
		public EnzymaticReactionContext(ReactionContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitEnzymaticReaction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReactionContext reaction() throws RecognitionException {
		ReactionContext _localctx = new ReactionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_reaction);
		int _la;
		try {
			setState(185);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				_localctx = new EnzymaticReactionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(121); complex();
				setState(123);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(122); match(SPACETABS);
					}
				}

				setState(125); match(11);
				setState(127);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(126); match(SPACETABS);
					}
				}

				setState(129); complex();
				setState(131);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(130); match(SPACETABS);
					}
				}

				setState(133); match(1);
				setState(135);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(134); match(SPACETABS);
					}
				}

				setState(137); complex();
				}
				break;

			case 2:
				_localctx = new BindingReactionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(139); complex();
				setState(141);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(140); match(SPACETABS);
					}
				}

				setState(143); match(7);
				setState(145);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(144); match(SPACETABS);
					}
				}

				setState(147); complex();
				setState(149);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(148); match(SPACETABS);
					}
				}

				setState(151); match(2);
				setState(153);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(152); match(SPACETABS);
					}
				}

				setState(155); complex();
				}
				break;

			case 3:
				_localctx = new UnbindingReactionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(157); complex();
				setState(159);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(158); match(SPACETABS);
					}
				}

				setState(161); match(2);
				setState(163);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(162); match(SPACETABS);
					}
				}

				setState(165); complex();
				setState(167);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(166); match(SPACETABS);
					}
				}

				setState(169); match(7);
				setState(171);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(170); match(SPACETABS);
					}
				}

				setState(173); complex();
				}
				break;

			case 4:
				_localctx = new ConversionReactionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(175); complex();
				setState(177);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(176); match(SPACETABS);
					}
				}

				setState(179); match(2);
				setState(181);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(180); match(SPACETABS);
					}
				}

				setState(183); complex();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComplexContext extends ParserRuleContext {
		public ComplexContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_complex; }
	 
		public ComplexContext() { }
		public void copyFrom(ComplexContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ComplexListContext extends ComplexContext {
		public SpeciesContext species() {
			return getRuleContext(SpeciesContext.class,0);
		}
		public ComplexContext complex() {
			return getRuleContext(ComplexContext.class,0);
		}
		public ComplexListContext(ComplexContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitComplexList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComplexRuleContext extends ComplexContext {
		public SpeciesContext species() {
			return getRuleContext(SpeciesContext.class,0);
		}
		public ComplexRuleContext(ComplexContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitComplexRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComplexContext complex() throws RecognitionException {
		ComplexContext _localctx = new ComplexContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_complex);
		try {
			setState(191);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				_localctx = new ComplexRuleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(187); species();
				}
				break;

			case 2:
				_localctx = new ComplexListContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(188); species();
				setState(189); complex();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpeciesContext extends ParserRuleContext {
		public SpeciesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_species; }
	 
		public SpeciesContext() { }
		public void copyFrom(SpeciesContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SpeciesRuleEmptyPropertiesContext extends SpeciesContext {
		public TerminalNode SPACETABS() { return getToken(motifGeneratorParser.SPACETABS, 0); }
		public TerminalNode ID() { return getToken(motifGeneratorParser.ID, 0); }
		public SpeciesRuleEmptyPropertiesContext(SpeciesContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitSpeciesRuleEmptyProperties(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SpeciesRuleContext extends SpeciesContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public IdListContext idList() {
			return getRuleContext(IdListContext.class,0);
		}
		public TerminalNode ID() { return getToken(motifGeneratorParser.ID, 0); }
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public SpeciesRuleContext(SpeciesContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitSpeciesRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SpeciesContext species() throws RecognitionException {
		SpeciesContext _localctx = new SpeciesContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_species);
		int _la;
		try {
			setState(210);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				_localctx = new SpeciesRuleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(193); match(ID);
				setState(194); match(9);
				setState(196);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(195); match(SPACETABS);
					}
				}

				setState(198); idList();
				setState(200);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(199); match(SPACETABS);
					}
				}

				setState(202); match(4);
				}
				break;

			case 2:
				_localctx = new SpeciesRuleEmptyPropertiesContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(204); match(ID);
				setState(205); match(9);
				setState(207);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(206); match(SPACETABS);
					}
				}

				setState(209); match(4);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdListContext extends ParserRuleContext {
		public IdListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idList; }
	 
		public IdListContext() { }
		public void copyFrom(IdListContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IdListTermContext extends IdListContext {
		public TerminalNode ID() { return getToken(motifGeneratorParser.ID, 0); }
		public IdListTermContext(IdListContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitIdListTerm(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdListListContext extends IdListContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public IdListContext idList() {
			return getRuleContext(IdListContext.class,0);
		}
		public TerminalNode ID() { return getToken(motifGeneratorParser.ID, 0); }
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public IdListListContext(IdListContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitIdListList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdListContext idList() throws RecognitionException {
		IdListContext _localctx = new IdListContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_idList);
		int _la;
		try {
			setState(222);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				_localctx = new IdListTermContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(212); match(ID);
				}
				break;

			case 2:
				_localctx = new IdListListContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(213); match(ID);
				setState(215);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(214); match(SPACETABS);
					}
				}

				setState(217); match(5);
				setState(219);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(218); match(SPACETABS);
					}
				}

				setState(221); idList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StrategyntContext extends ParserRuleContext {
		public StrategyntContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_strategynt; }
	 
		public StrategyntContext() { }
		public void copyFrom(StrategyntContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FuseTokenContext extends StrategyntContext {
		public TerminalNode FUSE() { return getToken(motifGeneratorParser.FUSE, 0); }
		public FuseTokenContext(StrategyntContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitFuseToken(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MergeTokenIOContext extends StrategyntContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public List<ComplexContext> complex() {
			return getRuleContexts(ComplexContext.class);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public TerminalNode MERGE() { return getToken(motifGeneratorParser.MERGE, 0); }
		public ComplexContext complex(int i) {
			return getRuleContext(ComplexContext.class,i);
		}
		public MergeTokenIOContext(StrategyntContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitMergeTokenIO(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MergeTokenContext extends StrategyntContext {
		public TerminalNode MERGE() { return getToken(motifGeneratorParser.MERGE, 0); }
		public MergeTokenContext(StrategyntContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitMergeToken(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultimergeTokenContext extends StrategyntContext {
		public TerminalNode MULTIMERGE() { return getToken(motifGeneratorParser.MULTIMERGE, 0); }
		public MultimergeTokenContext(StrategyntContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitMultimergeToken(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StrategyntContext strategynt() throws RecognitionException {
		StrategyntContext _localctx = new StrategyntContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_strategynt);
		int _la;
		try {
			setState(249);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				_localctx = new FuseTokenContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(224); match(FUSE);
				}
				break;

			case 2:
				_localctx = new MergeTokenContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(225); match(MERGE);
				}
				break;

			case 3:
				_localctx = new MultimergeTokenContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(226); match(MULTIMERGE);
				}
				break;

			case 4:
				_localctx = new MergeTokenIOContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(227); match(MERGE);
				setState(229);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(228); match(SPACETABS);
					}
				}

				setState(231); match(8);
				setState(233);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(232); match(SPACETABS);
					}
				}

				setState(235); match(3);
				setState(236); match(SPACETABS);
				setState(237); complex();
				setState(239);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(238); match(SPACETABS);
					}
				}

				setState(241); match(5);
				setState(243);
				_la = _input.LA(1);
				if (_la==SPACETABS) {
					{
					setState(242); match(SPACETABS);
					}
				}

				setState(245); match(6);
				setState(246); match(SPACETABS);
				setState(247); complex();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitContext extends ParserRuleContext {
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
	 
		public InitContext() { }
		public void copyFrom(InitContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class InitINTContext extends InitContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public TerminalNode INT() { return getToken(motifGeneratorParser.INT, 0); }
		public TerminalNode INIT() { return getToken(motifGeneratorParser.INIT, 0); }
		public ComplexContext complex() {
			return getRuleContext(ComplexContext.class,0);
		}
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public InitINTContext(InitContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitInitINT(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InitDOUBLEContext extends InitContext {
		public List<TerminalNode> SPACETABS() { return getTokens(motifGeneratorParser.SPACETABS); }
		public TerminalNode INIT() { return getToken(motifGeneratorParser.INIT, 0); }
		public ComplexContext complex() {
			return getRuleContext(ComplexContext.class,0);
		}
		public TerminalNode DOUBLE() { return getToken(motifGeneratorParser.DOUBLE, 0); }
		public TerminalNode SPACETABS(int i) {
			return getToken(motifGeneratorParser.SPACETABS, i);
		}
		public InitDOUBLEContext(InitContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof motifGeneratorVisitor ) return ((motifGeneratorVisitor<? extends T>)visitor).visitInitDOUBLE(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_init);
		try {
			setState(263);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				_localctx = new InitINTContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(251); match(INIT);
				setState(252); match(SPACETABS);
				setState(253); complex();
				setState(254); match(SPACETABS);
				setState(255); match(INT);
				}
				break;

			case 2:
				_localctx = new InitDOUBLEContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(257); match(INIT);
				setState(258); match(SPACETABS);
				setState(259); complex();
				setState(260); match(SPACETABS);
				setState(261); match(DOUBLE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\31\u010c\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\3\2\3\2\3\2\3\2\3\2\3\2\5\2\35\n\2\3\3\5\3 \n\3\3\3\3\3\3\3\5\3"+
		"%\n\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3-\n\3\3\3\3\3\3\3\3\3\5\3\63\n\3\3\3"+
		"\3\3\5\3\67\n\3\3\4\3\4\5\4;\n\4\3\4\3\4\5\4?\n\4\3\4\3\4\3\4\3\4\5\4"+
		"E\n\4\3\4\3\4\5\4I\n\4\3\4\3\4\3\4\3\4\5\4O\n\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\5\4X\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4a\n\4\3\4\3\4\5\4e\n\4"+
		"\3\4\3\4\5\4i\n\4\3\4\3\4\3\4\3\4\3\4\5\4p\n\4\3\5\3\5\3\5\5\5u\n\5\3"+
		"\5\3\5\3\5\5\5z\n\5\3\6\3\6\5\6~\n\6\3\6\3\6\5\6\u0082\n\6\3\6\3\6\5\6"+
		"\u0086\n\6\3\6\3\6\5\6\u008a\n\6\3\6\3\6\3\6\3\6\5\6\u0090\n\6\3\6\3\6"+
		"\5\6\u0094\n\6\3\6\3\6\5\6\u0098\n\6\3\6\3\6\5\6\u009c\n\6\3\6\3\6\3\6"+
		"\3\6\5\6\u00a2\n\6\3\6\3\6\5\6\u00a6\n\6\3\6\3\6\5\6\u00aa\n\6\3\6\3\6"+
		"\5\6\u00ae\n\6\3\6\3\6\3\6\3\6\5\6\u00b4\n\6\3\6\3\6\5\6\u00b8\n\6\3\6"+
		"\3\6\5\6\u00bc\n\6\3\7\3\7\3\7\3\7\5\7\u00c2\n\7\3\b\3\b\3\b\5\b\u00c7"+
		"\n\b\3\b\3\b\5\b\u00cb\n\b\3\b\3\b\3\b\3\b\3\b\5\b\u00d2\n\b\3\b\5\b\u00d5"+
		"\n\b\3\t\3\t\3\t\5\t\u00da\n\t\3\t\3\t\5\t\u00de\n\t\3\t\5\t\u00e1\n\t"+
		"\3\n\3\n\3\n\3\n\3\n\5\n\u00e8\n\n\3\n\3\n\5\n\u00ec\n\n\3\n\3\n\3\n\3"+
		"\n\5\n\u00f2\n\n\3\n\3\n\5\n\u00f6\n\n\3\n\3\n\3\n\3\n\5\n\u00fc\n\n\3"+
		"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u010a"+
		"\n\13\3\13\2\f\2\4\6\b\n\f\16\20\22\24\2\2\u0137\2\34\3\2\2\2\4\66\3\2"+
		"\2\2\6o\3\2\2\2\by\3\2\2\2\n\u00bb\3\2\2\2\f\u00c1\3\2\2\2\16\u00d4\3"+
		"\2\2\2\20\u00e0\3\2\2\2\22\u00fb\3\2\2\2\24\u0109\3\2\2\2\26\27\5\4\3"+
		"\2\27\30\7\2\2\3\30\35\3\2\2\2\31\32\5\4\3\2\32\33\5\2\2\2\33\35\3\2\2"+
		"\2\34\26\3\2\2\2\34\31\3\2\2\2\35\3\3\2\2\2\36 \7\21\2\2\37\36\3\2\2\2"+
		"\37 \3\2\2\2 !\3\2\2\2!\67\7\20\2\2\"$\5\6\4\2#%\7\21\2\2$#\3\2\2\2$%"+
		"\3\2\2\2%&\3\2\2\2&\'\7\20\2\2\'\67\3\2\2\2()\7\24\2\2)*\7\21\2\2*,\5"+
		"\22\n\2+-\7\21\2\2,+\3\2\2\2,-\3\2\2\2-.\3\2\2\2./\7\20\2\2/\67\3\2\2"+
		"\2\60\62\5\24\13\2\61\63\7\21\2\2\62\61\3\2\2\2\62\63\3\2\2\2\63\64\3"+
		"\2\2\2\64\65\7\20\2\2\65\67\3\2\2\2\66\37\3\2\2\2\66\"\3\2\2\2\66(\3\2"+
		"\2\2\66\60\3\2\2\2\67\5\3\2\2\28:\7\22\2\29;\7\21\2\2:9\3\2\2\2:;\3\2"+
		"\2\2;<\3\2\2\2<>\7\n\2\2=?\7\21\2\2>=\3\2\2\2>?\3\2\2\2?@\3\2\2\2@A\7"+
		"\5\2\2AB\7\21\2\2BD\5\f\7\2CE\7\21\2\2DC\3\2\2\2DE\3\2\2\2EF\3\2\2\2F"+
		"H\7\7\2\2GI\7\21\2\2HG\3\2\2\2HI\3\2\2\2IJ\3\2\2\2JK\7\b\2\2KL\7\21\2"+
		"\2LN\5\f\7\2MO\7\21\2\2NM\3\2\2\2NO\3\2\2\2OP\3\2\2\2PQ\7\20\2\2QR\5\b"+
		"\5\2RS\7\20\2\2ST\7\23\2\2Tp\3\2\2\2UW\7\22\2\2VX\7\21\2\2WV\3\2\2\2W"+
		"X\3\2\2\2XY\3\2\2\2YZ\7\20\2\2Z[\5\b\5\2[\\\7\20\2\2\\]\7\23\2\2]p\3\2"+
		"\2\2^`\7\22\2\2_a\7\21\2\2`_\3\2\2\2`a\3\2\2\2ab\3\2\2\2bd\7\f\2\2ce\7"+
		"\21\2\2dc\3\2\2\2de\3\2\2\2ef\3\2\2\2fh\7\31\2\2gi\7\21\2\2hg\3\2\2\2"+
		"hi\3\2\2\2ij\3\2\2\2jk\7\20\2\2kl\5\b\5\2lm\7\20\2\2mn\7\23\2\2np\3\2"+
		"\2\2o8\3\2\2\2oU\3\2\2\2o^\3\2\2\2p\7\3\2\2\2qz\5\n\6\2rt\5\n\6\2su\7"+
		"\21\2\2ts\3\2\2\2tu\3\2\2\2uv\3\2\2\2vw\7\20\2\2wx\5\b\5\2xz\3\2\2\2y"+
		"q\3\2\2\2yr\3\2\2\2z\t\3\2\2\2{}\5\f\7\2|~\7\21\2\2}|\3\2\2\2}~\3\2\2"+
		"\2~\177\3\2\2\2\177\u0081\7\r\2\2\u0080\u0082\7\21\2\2\u0081\u0080\3\2"+
		"\2\2\u0081\u0082\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0085\5\f\7\2\u0084"+
		"\u0086\7\21\2\2\u0085\u0084\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\3"+
		"\2\2\2\u0087\u0089\7\3\2\2\u0088\u008a\7\21\2\2\u0089\u0088\3\2\2\2\u0089"+
		"\u008a\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008c\5\f\7\2\u008c\u00bc\3\2"+
		"\2\2\u008d\u008f\5\f\7\2\u008e\u0090\7\21\2\2\u008f\u008e\3\2\2\2\u008f"+
		"\u0090\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u0093\7\t\2\2\u0092\u0094\7\21"+
		"\2\2\u0093\u0092\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0095\3\2\2\2\u0095"+
		"\u0097\5\f\7\2\u0096\u0098\7\21\2\2\u0097\u0096\3\2\2\2\u0097\u0098\3"+
		"\2\2\2\u0098\u0099\3\2\2\2\u0099\u009b\7\4\2\2\u009a\u009c\7\21\2\2\u009b"+
		"\u009a\3\2\2\2\u009b\u009c\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e\5\f"+
		"\7\2\u009e\u00bc\3\2\2\2\u009f\u00a1\5\f\7\2\u00a0\u00a2\7\21\2\2\u00a1"+
		"\u00a0\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a5\7\4"+
		"\2\2\u00a4\u00a6\7\21\2\2\u00a5\u00a4\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6"+
		"\u00a7\3\2\2\2\u00a7\u00a9\5\f\7\2\u00a8\u00aa\7\21\2\2\u00a9\u00a8\3"+
		"\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ad\7\t\2\2\u00ac"+
		"\u00ae\7\21\2\2\u00ad\u00ac\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00af\3"+
		"\2\2\2\u00af\u00b0\5\f\7\2\u00b0\u00bc\3\2\2\2\u00b1\u00b3\5\f\7\2\u00b2"+
		"\u00b4\7\21\2\2\u00b3\u00b2\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\u00b5\3"+
		"\2\2\2\u00b5\u00b7\7\4\2\2\u00b6\u00b8\7\21\2\2\u00b7\u00b6\3\2\2\2\u00b7"+
		"\u00b8\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00ba\5\f\7\2\u00ba\u00bc\3\2"+
		"\2\2\u00bb{\3\2\2\2\u00bb\u008d\3\2\2\2\u00bb\u009f\3\2\2\2\u00bb\u00b1"+
		"\3\2\2\2\u00bc\13\3\2\2\2\u00bd\u00c2\5\16\b\2\u00be\u00bf\5\16\b\2\u00bf"+
		"\u00c0\5\f\7\2\u00c0\u00c2\3\2\2\2\u00c1\u00bd\3\2\2\2\u00c1\u00be\3\2"+
		"\2\2\u00c2\r\3\2\2\2\u00c3\u00c4\7\31\2\2\u00c4\u00c6\7\13\2\2\u00c5\u00c7"+
		"\7\21\2\2\u00c6\u00c5\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c8\3\2\2\2"+
		"\u00c8\u00ca\5\20\t\2\u00c9\u00cb\7\21\2\2\u00ca\u00c9\3\2\2\2\u00ca\u00cb"+
		"\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00cd\7\6\2\2\u00cd\u00d5\3\2\2\2\u00ce"+
		"\u00cf\7\31\2\2\u00cf\u00d1\7\13\2\2\u00d0\u00d2\7\21\2\2\u00d1\u00d0"+
		"\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d5\7\6\2\2\u00d4"+
		"\u00c3\3\2\2\2\u00d4\u00ce\3\2\2\2\u00d5\17\3\2\2\2\u00d6\u00e1\7\31\2"+
		"\2\u00d7\u00d9\7\31\2\2\u00d8\u00da\7\21\2\2\u00d9\u00d8\3\2\2\2\u00d9"+
		"\u00da\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dd\7\7\2\2\u00dc\u00de\7\21"+
		"\2\2\u00dd\u00dc\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00df\3\2\2\2\u00df"+
		"\u00e1\5\20\t\2\u00e0\u00d6\3\2\2\2\u00e0\u00d7\3\2\2\2\u00e1\21\3\2\2"+
		"\2\u00e2\u00fc\7\27\2\2\u00e3\u00fc\7\25\2\2\u00e4\u00fc\7\26\2\2\u00e5"+
		"\u00e7\7\25\2\2\u00e6\u00e8\7\21\2\2\u00e7\u00e6\3\2\2\2\u00e7\u00e8\3"+
		"\2\2\2\u00e8\u00e9\3\2\2\2\u00e9\u00eb\7\n\2\2\u00ea\u00ec\7\21\2\2\u00eb"+
		"\u00ea\3\2\2\2\u00eb\u00ec\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed\u00ee\7\5"+
		"\2\2\u00ee\u00ef\7\21\2\2\u00ef\u00f1\5\f\7\2\u00f0\u00f2\7\21\2\2\u00f1"+
		"\u00f0\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f5\7\7"+
		"\2\2\u00f4\u00f6\7\21\2\2\u00f5\u00f4\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6"+
		"\u00f7\3\2\2\2\u00f7\u00f8\7\b\2\2\u00f8\u00f9\7\21\2\2\u00f9\u00fa\5"+
		"\f\7\2\u00fa\u00fc\3\2\2\2\u00fb\u00e2\3\2\2\2\u00fb\u00e3\3\2\2\2\u00fb"+
		"\u00e4\3\2\2\2\u00fb\u00e5\3\2\2\2\u00fc\23\3\2\2\2\u00fd\u00fe\7\30\2"+
		"\2\u00fe\u00ff\7\21\2\2\u00ff\u0100\5\f\7\2\u0100\u0101\7\21\2\2\u0101"+
		"\u0102\7\16\2\2\u0102\u010a\3\2\2\2\u0103\u0104\7\30\2\2\u0104\u0105\7"+
		"\21\2\2\u0105\u0106\5\f\7\2\u0106\u0107\7\21\2\2\u0107\u0108\7\17\2\2"+
		"\u0108\u010a\3\2\2\2\u0109\u00fd\3\2\2\2\u0109\u0103\3\2\2\2\u010a\25"+
		"\3\2\2\2\61\34\37$,\62\66:>DHNW`dhoty}\u0081\u0085\u0089\u008f\u0093\u0097"+
		"\u009b\u00a1\u00a5\u00a9\u00ad\u00b3\u00b7\u00bb\u00c1\u00c6\u00ca\u00d1"+
		"\u00d4\u00d9\u00dd\u00e0\u00e7\u00eb\u00f1\u00f5\u00fb\u0109";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}