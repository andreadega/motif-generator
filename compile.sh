#!/bin/bash

cd src/antlr
java -jar /usr/local/lib/antlr-4.1-complete.jar -no-listener -visitor -package antlr motifGenerator.g4
cd ../..

mkdir -p classes

javac -Xlint -d classes -cp lib/antlr-4.1-complete.jar src/motifGenerator/*.java src/com/scottlogic/util/SortedList.java src/antlr/*.java 

#make the jar

cd classes
jar -xf ../lib/antlr-4.1-complete.jar org
cd ..
jar cfm motifGenerator.jar Manifest.txt -C classes/ .
cd classes
rm -r org
rm -r com
rm -r motifGenerator
rm -r antlr
cd ..
mv motifGenerator.jar build/

#java -cp classes:lib/antlr-4.1-complete.jar motifGenerator.Main



#cd  motifs
#./dotAll.sh
