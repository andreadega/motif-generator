// Generated from motifGenerator.g4 by ANTLR 4.1
package antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import java.util.*;
import java.io.*;
import motifGenerator.*;

/**
 * This class provides an empty implementation of {@link motifGeneratorVisitor},
 * which can be extended to create a visitor which only needs to handle a subset
 * of the available methods.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public class MGVisitor extends motifGeneratorBaseVisitor<Object> {
  
  private Options opts = null;
  
  public MGVisitor(Options opts){
    this.opts = opts;
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitCodeRule(@NotNull motifGeneratorParser.CodeRuleContext ctx) {
    return visit(ctx.line());  
  }


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitBindingReaction(@NotNull motifGeneratorParser.BindingReactionContext ctx) {
    
    Complex A = (Complex) visit(ctx.complex(0));
    Complex B = (Complex) visit(ctx.complex(1));
    Complex C = (Complex) visit(ctx.complex(2));
    return new BindingReaction(A,B,C);
    
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitModuleLine(@NotNull motifGeneratorParser.ModuleLineContext ctx) { 
    return visit(ctx.module());
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitReactionRule(@NotNull motifGeneratorParser.ReactionRuleContext ctx) { 
    Set<Reaction> reactionsSet = new HashSet<Reaction>();
    Reaction r = (Reaction) visit(ctx.reaction());
    reactionsSet.add(r);
    return reactionsSet;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitComplexRule(@NotNull motifGeneratorParser.ComplexRuleContext ctx) {
    Species s = (Species) visit(ctx.species());
    Complex c = new Complex();
    c.add(s);
    return c;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSkipstuff(@NotNull motifGeneratorParser.SkipstuffContext ctx) {
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitModuleRule(@NotNull motifGeneratorParser.ModuleRuleContext ctx) { 
    @SuppressWarnings("unchecked")
    Set<Reaction> reactionsSet = (Set<Reaction>) visit(ctx.reactions());
    Set<Species> speciesSet = getSpeciesSetFromReactionSet(reactionsSet);
    Module m = new Module(speciesSet,reactionsSet);
    opts.addModuleToMainModuleList(m);
    return null;
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitModuleRuleGroup(@NotNull motifGeneratorParser.ModuleRuleGroupContext ctx) { 
    @SuppressWarnings("unchecked")
    Set<Reaction> reactionsSet = (Set<Reaction>) visit(ctx.reactions());
    Set<Species> speciesSet = getSpeciesSetFromReactionSet(reactionsSet);
    String groupID = ctx.ID().getText();
    Module m = new Module(speciesSet,reactionsSet);
    opts.addModuleToGroup(m,groupID);
    return null;
  }

  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitModuleRuleIO(@NotNull motifGeneratorParser.ModuleRuleIOContext ctx) { 
    @SuppressWarnings("unchecked")
    Set<Reaction> reactionsSet = (Set<Reaction>) visit(ctx.reactions());
    Set<Species> speciesSet = getSpeciesSetFromReactionSet(reactionsSet);
    Module m = new Module(speciesSet,reactionsSet);
    m.setInput((Complex)visit(ctx.complex(0)));
    m.setOutput((Complex)visit(ctx.complex(1)));
    opts.addModuleToMainModuleList(m);
    return null;
  }


	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitReactionList(@NotNull motifGeneratorParser.ReactionListContext ctx) { 
    @SuppressWarnings("unchecked")
    Set<Reaction> reactionsSet = (Set<Reaction>) visit(ctx.reactions());
    Reaction r = (Reaction) visit(ctx.reaction());
    reactionsSet.add(r);
    return reactionsSet;  
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitUnbindingReaction(@NotNull motifGeneratorParser.UnbindingReactionContext ctx) { 
    Complex A = (Complex) visit(ctx.complex(0));
    Complex B = (Complex) visit(ctx.complex(1));
    Complex C = (Complex) visit(ctx.complex(2));
    return new UnBindingReaction(A,B,C);    
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitConversionReaction(@NotNull motifGeneratorParser.ConversionReactionContext ctx) { 
    Complex A = (Complex) visit(ctx.complex(0));
    Complex B = (Complex) visit(ctx.complex(1));
    return new ConversionReaction(A,B);     
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitComplexList(@NotNull motifGeneratorParser.ComplexListContext ctx) { 
    Species s = (Species) visit(ctx.species());
    Complex c = (Complex) visit(ctx.complex());
    c.add(s);
    return c;
  }





	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitCodeList(@NotNull motifGeneratorParser.CodeListContext ctx) {
    visit(ctx.code());
    visit(ctx.line());
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitEnzymaticReaction(@NotNull motifGeneratorParser.EnzymaticReactionContext ctx) {
    Complex A = (Complex) visit(ctx.complex(0));
    Complex E = (Complex) visit(ctx.complex(1));
    Complex B = (Complex) visit(ctx.complex(2));
    return new EnzymaticReaction(A,E,B); 
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSpeciesRule(@NotNull motifGeneratorParser.SpeciesRuleContext ctx) {
    @SuppressWarnings("unchecked")
    Set<String> setOfProperties = (Set<String>) visit(ctx.idList());
    String id = ctx.ID().getText();
    return new Species(id,setOfProperties);
  }
  
/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitSpeciesRuleEmptyProperties(@NotNull motifGeneratorParser.SpeciesRuleEmptyPropertiesContext ctx) {
    Set<String> setOfProperties = new HashSet<String>();
    String id = ctx.ID().getText();
    return new Species(id,setOfProperties);
  }

  
  /**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitStrategyLine(@NotNull motifGeneratorParser.StrategyLineContext ctx) { 
    
    opts.setStrategy((String) visit(ctx.strategynt()));
    return null; 
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitMergeToken(@NotNull motifGeneratorParser.MergeTokenContext ctx) {
    return "merge";
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitMultimergeToken(@NotNull motifGeneratorParser.MultimergeTokenContext ctx) {
    return "multimerge";
  }

  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitMergeTokenIO(@NotNull motifGeneratorParser.MergeTokenIOContext ctx) { 
    opts.setInput((Complex)visit(ctx.complex(0)));
    opts.setOutput((Complex)visit(ctx.complex(1)));
    return "merge";
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitFuseToken(@NotNull motifGeneratorParser.FuseTokenContext ctx) {
    return "fuse"; 
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitIdListList(@NotNull motifGeneratorParser.IdListListContext ctx) {
    @SuppressWarnings("unchecked")
    Set<String> propertyList = (Set<String>) visit(ctx.idList());
    String tmp = ctx.ID().getText();
    propertyList.add(tmp);
    return propertyList;
    
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitIdListTerm(@NotNull motifGeneratorParser.IdListTermContext ctx) {
    Set<String> propertyList = new HashSet<String>();
    String tmp = ctx.ID().getText();
    propertyList.add(tmp);
    return propertyList;
    
  }
  
	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInitINT(@NotNull motifGeneratorParser.InitINTContext ctx) { 
    
    opts.setInitialConc((Complex) visit(ctx.complex()), Double.valueOf(ctx.INT().getText()));
    
    return null;
  }

	/**
	 * {@inheritDoc}
	 * <p/>
	 * The default implementation returns the result of calling
	 * {@link #visitChildren} on {@code ctx}.
	 */
	@Override public Object visitInitDOUBLE(@NotNull motifGeneratorParser.InitDOUBLEContext ctx) { 
    opts.setInitialConc((Complex) visit(ctx.complex()), Double.valueOf(ctx.DOUBLE().getText()));
    
    return null;
  }
  
  
  private Set<Species> getSpeciesSetFromReactionSet(Set<Reaction> reactionSet){
    Set<Species> speciesSet = new HashSet<Species>();
    Iterator<Reaction> ite = reactionSet.iterator();
    while(ite.hasNext()){
      Reaction r = ite.next();
      if (r instanceof EnzymaticReaction){
        EnzymaticReaction er = (EnzymaticReaction) r;
        speciesSet.addAll(er.getSubstrate().getMembers());
        speciesSet.addAll(er.getEnzyme().getMembers());
        speciesSet.addAll(er.getProduct().getMembers());
      } else if (r instanceof BindingReaction){
        BindingReaction br = (BindingReaction) r;
        speciesSet.addAll(br.getA().getMembers());
        speciesSet.addAll(br.getB().getMembers());
        speciesSet.addAll(br.getC().getMembers());
      } else if (r instanceof UnBindingReaction){
        UnBindingReaction ur = (UnBindingReaction) r;
        speciesSet.addAll(ur.getA().getMembers());
        speciesSet.addAll(ur.getB().getMembers());
        speciesSet.addAll(ur.getC().getMembers());        
      } else if (r instanceof ConversionReaction){
        ConversionReaction cr = (ConversionReaction) r;
        speciesSet.addAll(cr.getA().getMembers());
        speciesSet.addAll(cr.getB().getMembers());        
      } else {
        System.out.println("MGVisitor Class error: Unknown Reaction type.");
      }
    }
    return speciesSet;
  }
}
