package motifGenerator;

import java.util.*;

public class Options {
  
  public Options(){
    //empty
  }
  
 //main list of modules, all the modules in this list will be used for merging
  private LinkedList<Module> mainModuleList = new LinkedList<Module>(); 
  
  //These are module groups. Each group is identified by a String ID
  //Module groups are optionals and imply that only one module from
  //each group can be used for merging
  private HashMap<String,LinkedList<Module>> groups = new HashMap<String,LinkedList<Module>>();
  
  private String strategy = "";
  private Map<Complex,Double> initialConcentrations = new HashMap<Complex,Double>();
  private Complex input = null;
  private Complex output = null;
  
    
  public void addModuleToMainModuleList(Module toAdd){
    mainModuleList.addFirst(toAdd);
  }
    /**
   * return the main module list (lists of modules)
   * this list could be empty! If empty then the merge could just use 
   * modules from groups
   * */
  public LinkedList<Module> getMainModuleList(){
    return this.mainModuleList;
  }
  
  public void addModuleToGroup(Module toAdd, String group){
    LinkedList<Module> mlist = this.groups.get(group);
    if (mlist == null){
      //no group with this name exists, create it then!
      mlist = new LinkedList<Module>();
      mlist.addFirst(toAdd);
      groups.put(group,mlist);
    } else {
      //this group exists, add the module to the corresponding list
      mlist.addFirst(toAdd);
    }
  }

  
  /**
   * return the groups (lists of modules)
   * this list could be empty! It is empty if no groups
   * have been specified
   * */
  
  public HashMap<String,LinkedList<Module>> getGroups(){
    return this.groups;
  }
  
  public String getStrategy(){
    return strategy;
  }

  public void setStrategy(String strategy){
    this.strategy = strategy;
  }
  
  public double getInitialConc(Complex c){
    double initialConc = 0.0;
    if (initialConcentrations.containsKey(c)){
      initialConc = (initialConcentrations.get(c)).doubleValue();
    }
    return initialConc;
  }

  public double getInitialConc(Species s){
    double initialConc = 0.0;
    Complex c = new Complex();
    c.add(s);
    if (initialConcentrations.containsKey(c)){
      initialConc = (initialConcentrations.get(c)).doubleValue();
    }
    return initialConc;
  }
  
  public Map<Complex,Double> getInitialConcMap(){
    return initialConcentrations;
  }
  
  public void setInitialConc(Complex c, Double d){
      initialConcentrations.put(c,d);
  }
  
  public Complex getInput(){
    return input;
  }

  public Complex getOutput(){
    return output;
  }
  
  public void setInput(Complex input){
    this.input = input;
  }

  public void setOutput(Complex output){
    this.output = output;
  }
}
