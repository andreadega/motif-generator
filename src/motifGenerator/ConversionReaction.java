
package motifGenerator;

import java.util.*;

/**
 * Class for the reaction A -> B
 * */

public class ConversionReaction extends Reaction{
  
  private Complex A;
  private Complex B;
  
  /**
   * Constructor stores pointers to the Complex objects. Attributes of
   * the Complex objects can be altered by other parts of the program,
   * such as in case of Species substitution in a Module
   * */
  
  public ConversionReaction(Complex A, Complex B){
    this.A = A;
    this.B = B;
  }
  
  /**
   * Constructor for cloning an ConversionReaction object. This cloning
   * DEEP CLONES the Complex objects
   * */
  
  public ConversionReaction(ConversionReaction er){
    this.A = new Complex(er.A);
    this.B = new Complex(er.B);
  }
  
  @Override
  public Reaction deepCopy(){
    return new ConversionReaction(this);
  }
  
  @Override
  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof ConversionReaction))return false;
    ConversionReaction er = (ConversionReaction)other;  //downcast
    if (this.A.equals(er.A) && this.B.equals(er.B)) return true;
    return false;
  }
  
  @Override
  public int hashCode(){
    int hash = 17;
    hash += 19 * (null == A ? 0 : A.hashCode());
    hash += 23 * (null == B ? 0 : B.hashCode());
    return hash;
  } 
  
  public void setA(Complex A){
    this.A = A;
  }
  
  public Complex getA(){
    return this.A;
  }
  
  public void setB(Complex B){
    this.B = B;
  }
  
  public Complex getB(){
    return this.B;
  }
  
  @Override
  public void substituteMember(Species oldS, Species newS){
    A.substituteMember(oldS,newS);
    B.substituteMember(oldS,newS);
  }

  @Override
  public String toString(){
    return this.getA().toString() +
           " --> " + this.getB().toString();
  }
  

  public Set<Complex> getComplexes(){
    Set<Complex> rs = new HashSet<Complex>();
    rs.add(A);
    rs.add(B);
    return rs;
  }
  
  @Override
  public String printToSBML(int reactionIndex){
    String returnString = "";
    returnString += "      <reaction id=\"R" + reactionIndex + "\" reversible=\"false\" fast=\"false\"  >\n";
    returnString += "        <listOfReactants>\n";
    returnString += "          <speciesReference species=\"" + this.getA().toStringIDformat() + "\" stoichiometry=\"1\" />\n"; //stoichiometry is constant
    returnString += "        </listOfReactants>\n";
    returnString += "        <listOfProducts>\n";
    returnString += "          <speciesReference species=\"" + this.getB().toStringIDformat() + "\" stoichiometry=\"2\" />\n"; //stoichiometry is constant
    returnString += "        </listOfProducts>\n";
    returnString += "        <kineticLaw>\n";
    returnString += "          <math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n";
    returnString += "            <apply><times/><cn>1</cn><ci>" + this.getA().toStringIDformat() + "</ci></apply>\n";
    returnString += "          </math>\n";
    returnString += "        </kineticLaw>\n";
    returnString += "      </reaction>\n";
    return returnString;
  }

  @Override
  public String printToDOTv2(DotReactionClasses drc, int reactionIndex){
    String returnString = "";
    returnString += "\"" + this.getA().toString() + "\" -> R" + reactionIndex + "[arrowhead=None,weight=10];\n";
    returnString += "R" + reactionIndex + "[label=\"\", fixedsize=\"false\", width=0, height=0, shape=none];\n";
    returnString += "R" + reactionIndex + " -> \"" + this.getB().toString() + "\"[weight=10];\n";
    //~ returnString += "R" + reactionIndex + " -> \"" + this.getC().toString() + "\"[weight=10];\n";
    //~ returnString += "\"" + this.getEnzyme().toString() + "\" -> R" + reactionIndex + "[style=dotted,arrowhead=odot];\n";
    //~ returnString += "\"" + er.getSubstrate().toString() + "\" -> \"" + er.getProduct().toString() + "\" [style=invis];\n";
    
    //~ returnString += "{ rank = same; \"" + er.getSubstrate().toString() + "\"; \"" + er.getProduct().toString() + "\"};\n";

    drc.add(this.getA(),this.getB(),"R" + reactionIndex);
    //~ drc.add(this.getA(),this.getC(),"R" + reactionIndex);
    return returnString;
  }
  
}
