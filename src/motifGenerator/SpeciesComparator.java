
package motifGenerator;

import java.util.*;

public class SpeciesComparator implements Comparator<Species> {

  @Override
  public int compare(Species s1, Species s2){
    return s1.toString().compareTo(s2.toString());
  }
  
}
