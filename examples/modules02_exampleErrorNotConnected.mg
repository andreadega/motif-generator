module
A(ik) -[ KA(ak) ]-> pA(ak)
pA(ak) -[ PA(ap) ]-> A(ik)
endmodule

module
B(ik,ik2) + C(ak,ak2) -> B(ik,ik2)C(ak,ak2)
B(ik,ik2)C(ak,ak2) -> B(ik,ik2) + C(ak,ak2)
endmodule

module
D() -[ KD() ]-> pD()
pD() -[ PD() ]-> D()
endmodule

strategy merge
