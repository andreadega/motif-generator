
package motifGenerator;

import java.util.*;

/**
 * Class for the reaction S -[E]-> P
 * */

public class EnzymaticReaction extends Reaction{
  
  private Complex S;
  private Complex P;
  private Complex E;
  
  /**
   * Constructor stores pointers to the Complex objects. Attributes of
   * the Complex objects can be altered by other parts of the program,
   * such as in case of Species substitution in a Module
   * */
  
  public EnzymaticReaction(Complex S, Complex E, Complex P){
    this.S = S;
    this.P = P;
    this.E = E;
  }
  
  public Set<Complex> getComplexes(){
    Set<Complex> rs = new HashSet<Complex>();
    rs.add(S);
    rs.add(P);
    rs.add(E);
    return rs;
  }
  
  /**
   * Constructor for cloning an EnzymaticReaction object. This cloning
   * DEEP CLONES the Complex objects, but only copies the pointers
   * */
  
  public EnzymaticReaction(EnzymaticReaction er){
    this.S = new Complex(er.S);
    this.P = new Complex(er.P);
    this.E = new Complex(er.E);
  }
  
  @Override
  public Reaction deepCopy(){
    return new EnzymaticReaction(this);
  }
  
  @Override
  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof EnzymaticReaction))return false;
    EnzymaticReaction er = (EnzymaticReaction)other;  //downcast
    if (this.S.equals(er.S) && this.P.equals(er.P) && this.E.equals(er.E)) return true;
    return false;
  }
  
  @Override
  public int hashCode(){
    int hash = 17;
    hash += 19 * (null == S ? 0 : S.hashCode());
    hash += 23 * (null == P ? 0 : P.hashCode());
    hash += 29 * (null == E ? 0 : E.hashCode());
    return hash;
  } 
  
  public void setSubstrate(Complex S){
    this.S = S;
  }
  
  public Complex getSubstrate(){
    return this.S;
  }
  
  public void setProduct(Complex P){
    this.P = P;
  }
  
  public Complex getProduct(){
    return this.P;
  }

  public void setEnzyme(Complex E){
    this.E = E;
  }
  
  public Complex getEnzyme(){
    return this.E;
  }
  
  @Override
  public void substituteMember(Species oldS, Species newS){
    S.substituteMember(oldS,newS);
    P.substituteMember(oldS,newS);
    E.substituteMember(oldS,newS);
  }
  
  @Override
  public String toString(){
    return this.getSubstrate().toString() +
           " --[" + this.getEnzyme().toString() +
           "]--> " + this.getProduct().toString();
  }
  
  @Override
  public String printToSBML(int reactionIndex){
    String returnString = "";
    returnString += "      <reaction id=\"R" + reactionIndex + "\" reversible=\"false\" fast=\"false\"  >\n";
    returnString += "        <listOfReactants>\n";
    returnString += "          <speciesReference species=\"" + this.getSubstrate().toStringIDformat() + "\" stoichiometry=\"1\" />\n"; //stoichiometry is constant
    returnString += "        </listOfReactants>\n";
    returnString += "        <listOfProducts>\n";
    returnString += "          <speciesReference species=\"" + this.getProduct().toStringIDformat() + "\" stoichiometry=\"1\" />\n"; //stoichiometry is constant
    returnString += "        </listOfProducts>\n";
    returnString += "        <listOfModifiers>\n";
    returnString += "          <modifierSpeciesReference species=\"" + this.getEnzyme().toStringIDformat() + "\"/>\n"; 
    returnString += "        </listOfModifiers>\n";
    returnString += "        <kineticLaw>\n";
    returnString += "          <math xmlns=\"http://www.w3.org/1998/Math/MathML\">\n";
    returnString += "            <apply><divide/><apply><times/><cn>1</cn><ci>" + this.getSubstrate().toStringIDformat() + "</ci><ci>" + this.getEnzyme().toStringIDformat() + "</ci></apply><apply><plus/><cn>0.5</cn><ci>" + this.getSubstrate().toStringIDformat() + "</ci></apply></apply>\n";
    returnString += "          </math>\n";
    returnString += "        </kineticLaw>\n";
    returnString += "      </reaction>\n";
    return returnString;
  }
  
  @Override
  public String printToDOTv2(DotReactionClasses drc, int reactionIndex){
    String returnString = "";
    returnString += "\"" + this.getSubstrate().toString() + "\" -> R" + reactionIndex + "[arrowhead=None,weight=10,color=green];\n";
    returnString += "R" + reactionIndex + "[label=\"\", fixedsize=\"false\", width=0, height=0, shape=none];\n";
    returnString += "R" + reactionIndex + " -> \"" + this.getProduct().toString() + "\"[weight=10,color=green];\n";
    returnString += "\"" + this.getEnzyme().toString() + "\" -> R" + reactionIndex + "[style=dotted,arrowhead=odot,color=green];\n";
    //~ returnString += "\"" + er.getSubstrate().toString() + "\" -> \"" + er.getProduct().toString() + "\" [style=invis];\n";
    
    //~ returnString += "{ rank = same; \"" + er.getSubstrate().toString() + "\"; \"" + er.getProduct().toString() + "\"};\n";

    drc.add(this.getSubstrate(),this.getProduct(),"R" + reactionIndex);
    return returnString;
  }
}
