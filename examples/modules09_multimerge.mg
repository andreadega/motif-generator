
module
A() -[ KA(x1,x3) ]-> pA()
pA() -[ PA(y1,x3) ]-> A()
endmodule

module
B(x1,x2,x3) + C(y1,y2,y3) -> B(x1,x2,x3)C(y1,y2,y3)
B(x1,x2,x3)C(y1,y2,y3) -> B(x1,x2,x3) + C(y1,y2,y3)
endmodule

module
D() -[ KD(y3,x2) ]-> pD()
pD() -[ PD(y2,y3) ]-> D()
endmodule

strategy multimerge
