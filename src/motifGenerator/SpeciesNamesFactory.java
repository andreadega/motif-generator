
package motifGenerator;

import java.util.*;
import java.io.*;

public class SpeciesNamesFactory {
  private static int counter = 0;
  
  public static String getSpeciesName(){
    counter++;
    return "_S" + counter;
  }
}
