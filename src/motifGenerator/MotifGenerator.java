
package motifGenerator;

import java.util.*;

public class MotifGenerator {
  
  /**
   * check if any of the given modules share species. Number of checks 
   * between Modules is ((n-1)*n)/2, with n the number of Modules.  
   * */
  
  public static boolean modulesShareSpecies(LinkedList<Module> moduleList){
    
    if (moduleList == null) return false;
    if (moduleList.size() < 2) return false;
    Queue<Module> queue = new LinkedList<Module>();
    queue.addAll(moduleList);
    
    Module current; 
    while (!queue.isEmpty()){
      current = queue.poll(); //retrives and removes first element
      Iterator<Module> ite = queue.iterator();
      while (ite.hasNext()){
        Module tmp = ite.next();
        if (tmp.sharesSpeciesWith(current)) return true;
      }
    }
    return false;
  }
  
  
  /**
   * Merge enzymatic modules A and B, substituting a Species in one
   * module and a Species in the other module with a new common Species.
   * The substitution takes
   * place if the two species share a property from their property list,
   * and if they are not the same molecule. The new species has the 
   * union of the properties in the species
   * */
  
  public static LinkedList<Module> mergeModules(Module A, Module B, Options mgv){
    
    LinkedList<Module> returnList = new LinkedList<Module>();
    

    
    Iterator<Species> iteSA;
    Iterator<Species> iteSB;


    
    //add simple union //NO! don't! we assume no overlapping of species
    //tmpEM = new Module(A);
    //tmpEM.getSpeciesSet().addAll(B.getSpeciesSet());
    //tmpEM.getReactionsSet().addAll(B.getReactionsSet());
    
    //returnList.addFirst(tmpEM);
    
    iteSA = A.getSpeciesSet().iterator();
    while (iteSA.hasNext()){
      Species tmpSA = iteSA.next();
      iteSB = B.getSpeciesSet().iterator();
      while (iteSB.hasNext()){
        Species tmpSB = iteSB.next();
        //If tmpSA has the same role of tmpSB (shared properties)
        //then merge, replacing tmpSB and tmpSA with a new species taken
        //from the SpeciesNamesFactory, with the union of the property sets
        //(but not if they are the same molecule!)
        
        if(tmpSA.hasSameRoleOf(tmpSB) && !(tmpSA.equals(tmpSB))){
                    
          returnList.addAll(createMergedModule(A,B,tmpSA,tmpSB,mgv));
        }
      }
    }
    return returnList;
  }
  
  

  /**
   * Merge enzymatic modules A and B, merging as many compatible species
   * as possbile at once. 
   * Given two modules there is a set of matched pairs from one module to the other
   * Merged modules are obtained considering maximal matchings
   * A matched pair is when two species share a property from their property list,
   * and if they are not the same molecule. The new species has the 
   * union of the properties in the two species
   * */
  
  public static LinkedList<Module> multiMergeModules(Module A, Module B, Options mgv){
    
    LinkedList<Module> returnList = new LinkedList<Module>();
    

    
    Iterator<Species> iteSA;
    Iterator<Species> iteSB;

    //Obtain the set of all possible pair of compatible species
    Set<Pair<Species,Species>> setOfPairs = new HashSet<Pair<Species,Species>>();
    
    iteSA = A.getSpeciesSet().iterator();
    while (iteSA.hasNext()){
      Species tmpSA = iteSA.next();
      iteSB = B.getSpeciesSet().iterator();
      while (iteSB.hasNext()){
        Species tmpSB = iteSB.next();
        //If tmpSA has the same role of tmpSB (shared properties)
        //then it is a possible match, where we could replace tmpSB and tmpSA 
        //with a new species taken
        //from the SpeciesNamesFactory, with the union of the property sets
        //(but not if they are the same molecule!)
        
        if(tmpSA.hasSameRoleOf(tmpSB) && !(tmpSA.equals(tmpSB))){
                    
          setOfPairs.add(new Pair<Species,Species>(tmpSA,tmpSB));
        }
      }
    }
    
    //retrieve the set of maximal matches:
    Set<Set<Pair<Species,Species>>> mm = MatchingTool.getMaximalMatches(setOfPairs);
    
    //now create a new module for each maximal match
    Iterator<Set<Pair<Species,Species>>> ite = mm.iterator();
    while (ite.hasNext()){
      Set<Pair<Species,Species>> match = ite.next();
      
      LinkedList<Module> mod = createMergedModule(A,B,match,mgv);      
      returnList.addAll(mod);
      
    }
    
    
    
    
    return returnList;
  }
  
  /**
   * return a new Module, which is a merge of modules A and B, where tmpSA
   * and tmpSB are replaced by a new common Species. This function assumes that
   * tmpSA is in A and tmpSB is in B. Can return more than one module in case
   * initial concentration of merged species are not the same
   * */
  
  private static LinkedList<Module> createMergedModule(Module A, Module B, Species tmpSA, Species tmpSB, Options mgv){
    LinkedList<Module> returnList = new LinkedList<Module>();
    
    Iterator<Reaction> iteReA; 
    Iterator<Reaction> iteReB;
    Module tmpEM;
    Reaction tmpER;
    Set<Species> tmpSpeciesSet;
    Set<Reaction> tmpReactionSet;
    
    Set<String> setOfProperties = new HashSet<String>();
    setOfProperties.addAll(tmpSA.getSetOfProperties());
    setOfProperties.addAll(tmpSB.getSetOfProperties());
    Species newSpecies = new Species(SpeciesNamesFactory.getSpeciesName(),setOfProperties);
    
    
    //add species of modules A and B removing tmpSA and tmpSB and
    //then adding newSpecies
    tmpSpeciesSet = new HashSet<Species>();
    tmpSpeciesSet.addAll(B.getSpeciesSet());
    tmpSpeciesSet.addAll(A.getSpeciesSet());
    tmpSpeciesSet.remove(tmpSB);
    tmpSpeciesSet.remove(tmpSA);
    tmpSpeciesSet.add(newSpecies);
    
    
    //add reactions of A and B, replacing tmpSA and tmpSB with newSpecies
    tmpReactionSet = new HashSet<Reaction>();
    iteReA = A.getReactionsSet().iterator();
    while (iteReA.hasNext()){
      tmpER = iteReA.next().deepCopy(); //copy constructor
      tmpER.substituteMember(tmpSA,newSpecies); //substitution
      tmpReactionSet.add(tmpER);
    }
    
    iteReB = B.getReactionsSet().iterator();
    while (iteReB.hasNext()){
      tmpER = iteReB.next().deepCopy(); //copy constructor
      tmpER.substituteMember(tmpSB,newSpecies); //substitution
      tmpReactionSet.add(tmpER);
    }
                      
    tmpEM = new Module(tmpSpeciesSet,tmpReactionSet);
    
    //set the input and outputs of the new module
    
    Complex newInput = null;
    
    if (mgv.getInput() != null){
      newInput = new Complex(mgv.getInput());
      newInput.substituteMember(tmpSA,newSpecies);
      newInput.substituteMember(tmpSB,newSpecies);
    }
    
    Complex newOutput = null;
    if (mgv.getOutput() != null){
      newOutput = new Complex(mgv.getOutput());
      newOutput.substituteMember(tmpSA,newSpecies);
      newOutput.substituteMember(tmpSB,newSpecies);
    }
  
    tmpEM.setInput(newInput);
    tmpEM.setOutput(newOutput);
    
    //now need to set initial concentrations of the new module

    double initSA = mgv.getInitialConc(tmpSA);
    double initSB = mgv.getInitialConc(tmpSB);
    
    tmpEM.putAllInitialConc(mgv.getInitialConcMap());
    substituteInInitialConcMap(tmpEM,tmpSA,tmpSB,newSpecies,new Double(initSA));
    returnList.add(tmpEM); 
    
    
              
    if (initSA != initSB){
      // if the initial concentrations of SA and SB do not agree,
      // add another module with a different initial concentration
      
      Module tmpEM2 = new Module(tmpEM); //copy constructor
      Complex newSC = new Complex();
      newSC.add(newSpecies);
      tmpEM2.setInitialConc(newSC,new Double(initSB));
      returnList.add(tmpEM2);
      
    } 
    return returnList;
  }

  /**
   * return a new Module, which is a merge of modules A and B, where tmpSA
   * and iteSB are replaced by a new common Species. This function assumes that
   * iteSA is in A and iteSB is in B. Can return more than one module in case
   * initial concentration of merged species are not the same
   * */
  
  private static LinkedList<Module> createMergedModule(Module A, Module B, Set<Pair<Species,Species>> match, Options mgv){
    LinkedList<Module> returnList = new LinkedList<Module>();
    
    Iterator<Reaction> iteReA; 
    Iterator<Reaction> iteReB;
    Module tmpEM;
    Reaction tmpER;
    Set<Species> tmpSpeciesSet;
    Set<Reaction> tmpReactionSet;
    
    //---------- init
    
    //copy of species sets
    tmpSpeciesSet = new HashSet<Species>();
    tmpSpeciesSet.addAll(B.getSpeciesSet());
    tmpSpeciesSet.addAll(A.getSpeciesSet());
    
    //copy reaction sets
    tmpReactionSet = new HashSet<Reaction>();
    iteReA = A.getReactionsSet().iterator();
    while (iteReA.hasNext()){
      tmpER = iteReA.next().deepCopy(); //copy constructor
      tmpReactionSet.add(tmpER);
    }
    
    iteReB = B.getReactionsSet().iterator();
    while (iteReB.hasNext()){
      tmpER = iteReB.next().deepCopy(); //copy constructor
      tmpReactionSet.add(tmpER);
    }    
    
    // I/O
    Complex newInput = null;
    Complex newOutput = null;   
    if (mgv.getInput() != null) newInput = new Complex(mgv.getInput());
    if (mgv.getOutput() != null) newOutput = new Complex(mgv.getOutput()); 
    
    //----------------- loop on a pair of species tmpSA and tmpSB
    Iterator<Pair<Species,Species>> itePair = match.iterator();
    while (itePair.hasNext()){
      Pair<Species,Species> pair = itePair.next();
      
      Species tmpSA = pair.first;
      Species tmpSB = pair.second;
      
      Set<String> setOfProperties = new HashSet<String>();
      setOfProperties.addAll(tmpSA.getSetOfProperties());
      setOfProperties.addAll(tmpSB.getSetOfProperties());
      Species newSpecies = new Species(SpeciesNamesFactory.getSpeciesName(),setOfProperties);
      
      
      //add species of modules A and B removing tmpSA and tmpSB and
      //then adding newSpecies
      tmpSpeciesSet.remove(tmpSB);
      tmpSpeciesSet.remove(tmpSA);
      tmpSpeciesSet.add(newSpecies);
      
      
      //add replace tmpSA and tmpSB with newSpecies
      Iterator<Reaction> iteRe = tmpReactionSet.iterator();
      while (iteRe.hasNext()){
        tmpER = iteRe.next();
        tmpER.substituteMember(tmpSA,newSpecies); //substitution
        tmpER.substituteMember(tmpSB,newSpecies); //substitution
      }
      
      // I/O
      
      if (mgv.getInput() != null){
        newInput.substituteMember(tmpSA,newSpecies);
        newInput.substituteMember(tmpSB,newSpecies);
      }
      

      if (mgv.getOutput() != null){
        newOutput.substituteMember(tmpSA,newSpecies);
        newOutput.substituteMember(tmpSB,newSpecies);
      }
      
    }
    //----------------- end loop on a pair of species tmpSA and tmpSB
                      
    tmpEM = new Module(tmpSpeciesSet,tmpReactionSet);
    
    //set the input and outputs of the new module
  
    tmpEM.setInput(newInput);
    tmpEM.setOutput(newOutput);
    
    
    //TODO: INITIAL CONCENTRATIONS (ALL COMBINATIONS) Need to change 
    //Need to change functions so that I can prepare the initial conc map
    //before I instantiate the new Module tmpEM
    
    //~ //now need to set initial concentrations of the new module
//~ 
    //~ double initSA = mgv.getInitialConc(tmpSA);
    //~ double initSB = mgv.getInitialConc(tmpSB);
    //~ 
    //~ //initial concentrations
    //~ tmpEM.putAllInitialConc(mgv.getInitialConcMap());
    //~ substituteInInitialConcMap(tmpEM,tmpSA,tmpSB,newSpecies,new Double(initSA));
    returnList.add(tmpEM); 
    
    
    
    //TODO: DISCREPANCIES IN INITIAL CONCENTRATIONS (ALL COMBINATIONS)  
    
    //~ if (initSA != initSB){
      //~ // if the initial concentrations of SA and SB do not agree,
      //~ // add another module with a different initial concentration
      //~ 
      //~ Module tmpEM2 = new Module(tmpEM); //copy constructor
      //~ Complex newSC = new Complex();
      //~ newSC.add(newSpecies);
      //~ tmpEM2.setInitialConc(newSC,new Double(initSB));
      //~ returnList.add(tmpEM2);
      //~ 
    //~ } 
     
    
    return returnList;
  }


  
  /**
   * attempts to substitute species SA and SB in a map of initial 
   * concentration of complexes with newSpecies
   * Because SA and SB are both substituted by newSpecies, one needs to 
   * choose which initial concentration to use. This function will force
   * to use initNewSpecies
   * */
  
  private static void substituteInInitialConcMap(Module mod, 
                    Species SA, Species SB, Species newSpecies, Double initNewSpecies){
    Map<Complex,Double> m = mod.getInitialConcMap();
    Set<Complex> keys = new HashSet<Complex>(m.keySet());
    // In the line above I need to copy the key set to avoid ConcurrentModificationException
    Iterator<Complex> ite = keys.iterator();
    while (ite.hasNext()){
      Complex tmp = ite.next();
      if (tmp.hasMember(SA)){
        Double d = m.remove(tmp);
        Complex newKey = new Complex(tmp);
        newKey.substituteMember(SA,newSpecies);
        m.put(newKey,d);
      } else if (tmp.hasMember(SB)) {
        Double d = m.remove(tmp);
        Complex newKey = new Complex(tmp);
        newKey.substituteMember(SB,newSpecies);
        m.put(newKey,d);      
      }
    }
    //now just put (newSpecies,initNewSpecies) to overwrite newSpecies
    //mapping
    Complex newSC = new Complex();
    newSC.add(newSpecies);
    m.put(newSC,initNewSpecies);
    
  }

  
  /**
   * Merge a list of Modules 
   * 
   * returns null if nothing was merged
   * */
  
  public static LinkedList<Module> mergeModuleList(LinkedList<Module> moduleList, Options mgv) throws CannotMergeException{
      
        //put all the modules in a queue
        LinkedList<Module> deque = new LinkedList<Module>();
        deque.addAll(moduleList);
        
        LinkedList<Module> mergedModules1 = null;
        LinkedList<Module> mergedModules2 = null;
        
        Module A; //current module to merge
        Module tmp;
        
        if (deque.peekFirst() != null){
          //if not empty
          A = deque.pollFirst(); // return and remove from deque
        
          if (deque.peekFirst() != null){
            //initalise
            tmp = deque.pollFirst(); // return and remove from deque
            
            //ML0 = M1 + M2
            if (mgv.getStrategy().equals("merge")){
              mergedModules1 = mergeModules(tmp,A,mgv); 
            }else if (mgv.getStrategy().equals("multimerge")){
              mergedModules1 = multiMergeModules(tmp,A,mgv); 
            }else{
              throw new CannotMergeException("[error] Unknown motif generation strategy!");
            }
            int dequeSize = deque.size();
            int i = 0;
            while (mergedModules1.size() == 0 && i <= dequeSize){
              //as long as merging returns an empty list, put the current
              //module at the back of the deque and try with another one
              //up to a number of times as the elements in deque
              
              deque.addLast(tmp);
              tmp = deque.pollFirst(); // return and remove from deque
              //ML0 = M1 + M2
              if (mgv.getStrategy().equals("merge")){
                mergedModules1 = mergeModules(tmp,A,mgv); 
              }else if (mgv.getStrategy().equals("multimerge")){
                mergedModules1 = multiMergeModules(tmp,A,mgv); 
              }else{
                throw new CannotMergeException("[error] Unknown motif generation strategy!");
              }
              
              i++;
            }
            

            if(dequeSize < i){
              //if dequeSize < i then A is isolated! Should give an error!
              //as we assume that the relation "Mi can merge with Mj" should
              //form a graph that connects all modules.              
              
              //System.out.println("Found a Module that cannot merge any other module! this should be incorrect!");
              throw new CannotMergeException("[error] Found a Module that cannot merge any other module! this should be incorrect!");
              //return null;
            }
            
            while (deque.peekFirst() != null){ //until the deque is empty
              A = deque.pollFirst(); // get and remove Mi from deque, with i=3...n
              
              
              Iterator<Module> iteMM1 = mergedModules1.iterator();  
              
              //compute ML(i-2) = ML(i-3) x Mi
              if (iteMM1.hasNext()){  //this should always be true because
                                      //we assume that the relation "Mi can merge with Mj" should
                                      //form a graph that connects all modules. 
                //moreged module list is not empty
                tmp = iteMM1.next();
                if (mgv.getStrategy().equals("merge")){
                  mergedModules2 = mergeModules(tmp,A,mgv); 
                }else if (mgv.getStrategy().equals("multimerge")){
                  mergedModules2 = multiMergeModules(tmp,A,mgv); 
                }else{
                  throw new CannotMergeException("[error] Unknown motif generation strategy!");
                }
                dequeSize = deque.size();
                i = 0;
                while (mergedModules2.size() == 0 && i <= dequeSize){
                  //as long as merging returns an empty list, put the current
                  //module at the back of the deque and try with another one
                  //up to a number of times as the elements in deque
                  
                  deque.addLast(A);
                  A = deque.pollFirst(); // return and remove from deque
                  
                  if (mgv.getStrategy().equals("merge")){
                    mergedModules2 = mergeModules(tmp,A,mgv); 
                  }else if (mgv.getStrategy().equals("multimerge")){
                    mergedModules2 = multiMergeModules(tmp,A,mgv); 
                  }else{
                    throw new CannotMergeException("[error] Unknown motif generation strategy!");
                  }
                  
                  i++;
                }
                
    
                if(dequeSize < i){
                  //if dequeSize < i then tmp is isolated! Should give an error!
                  //as we assume that the relation "Mi can merge with Mj" should
                  //form a graph that connects all modules.              
                  
                  System.out.println("Found a Group of Modules that cannot merge any other module! this should be incorrect!");
                  return null;
                }
                
                //if tmp is not isolated so are all the modules generated so far with
                //the same modules used for tmp
                
                while (iteMM1.hasNext()){
                  tmp = iteMM1.next();
                  
                  if (mgv.getStrategy().equals("merge")){
                    mergedModules2.addAll(mergeModules(tmp,A,mgv));
                  }else if (mgv.getStrategy().equals("multimerge")){
                    mergedModules2.addAll(multiMergeModules(tmp,A,mgv));
                  }else{
                    throw new CannotMergeException("[error] Unknown motif generation strategy!");
                  }
                }
              
                // update merged modules, replace ML(i-3) with ML(i-2)
                mergedModules1 = mergedModules2; 
              }else{
                //merged module list is empty
              }
            }
          }else{
            System.out.println("Only one module found, nothing to merge!");
          }
        }else{
          System.out.println("Modules not found!");
        }
        return mergedModules1;
  }
  

}
