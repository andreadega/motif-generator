
module
A() -[ KA() ]-> pA(kinase)
pA(kinase) -[ PA() ]-> A()
endmodule

module
D() -[ KD(kinase) ]-> pD()
pD() -[ PD() ]-> D()
endmodule

strategy merge
