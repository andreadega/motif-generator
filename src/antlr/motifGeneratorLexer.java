// Generated from motifGenerator.g4 by ANTLR 4.1
package antlr;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class motifGeneratorLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__10=1, T__9=2, T__8=3, T__7=4, T__6=5, T__5=6, T__4=7, T__3=8, T__2=9, 
		T__1=10, T__0=11, INT=12, DOUBLE=13, NEWLINE=14, SPACETABS=15, MODULESTART=16, 
		MODULEEND=17, STRATEGY=18, MERGE=19, MULTIMERGE=20, FUSE=21, INIT=22, 
		ID=23;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"']->'", "'->'", "'input'", "')'", "','", "'output'", "'+'", "':'", "'('", 
		"'group'", "'-['", "INT", "DOUBLE", "NEWLINE", "SPACETABS", "'module'", 
		"'endmodule'", "'strategy'", "'merge'", "'multimerge'", "'fuse'", "'init'", 
		"ID"
	};
	public static final String[] ruleNames = {
		"T__10", "T__9", "T__8", "T__7", "T__6", "T__5", "T__4", "T__3", "T__2", 
		"T__1", "T__0", "INT", "DOUBLE", "NEWLINE", "SPACETABS", "MODULESTART", 
		"MODULEEND", "STRATEGY", "MERGE", "MULTIMERGE", "FUSE", "INIT", "ID"
	};


	public motifGeneratorLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "motifGenerator.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\2\31\u00af\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2"+
		"\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\f\3\f\3\f\3\r\6\rZ\n\r\r\r\16\r[\3\16\6\16_\n\16\r\16\16\16`\3"+
		"\16\3\16\7\16e\n\16\f\16\16\16h\13\16\3\17\5\17k\n\17\3\17\3\17\3\20\6"+
		"\20p\n\20\r\20\16\20q\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27"+
		"\3\27\3\30\3\30\7\30\u00ab\n\30\f\30\16\30\u00ae\13\30\2\31\3\3\1\5\4"+
		"\1\7\5\1\t\6\1\13\7\1\r\b\1\17\t\1\21\n\1\23\13\1\25\f\1\27\r\1\31\16"+
		"\1\33\17\1\35\20\1\37\21\1!\22\1#\23\1%\24\1\'\25\1)\26\1+\27\1-\30\1"+
		"/\31\1\3\2\6\3\2\62;\4\2\13\13\"\"\4\2C\\c|\7\2\62;C\\^^aac|\u00b4\2\3"+
		"\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2"+
		"\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31"+
		"\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2"+
		"\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\3"+
		"\61\3\2\2\2\5\65\3\2\2\2\78\3\2\2\2\t>\3\2\2\2\13@\3\2\2\2\rB\3\2\2\2"+
		"\17I\3\2\2\2\21K\3\2\2\2\23M\3\2\2\2\25O\3\2\2\2\27U\3\2\2\2\31Y\3\2\2"+
		"\2\33^\3\2\2\2\35j\3\2\2\2\37o\3\2\2\2!s\3\2\2\2#z\3\2\2\2%\u0084\3\2"+
		"\2\2\'\u008d\3\2\2\2)\u0093\3\2\2\2+\u009e\3\2\2\2-\u00a3\3\2\2\2/\u00a8"+
		"\3\2\2\2\61\62\7_\2\2\62\63\7/\2\2\63\64\7@\2\2\64\4\3\2\2\2\65\66\7/"+
		"\2\2\66\67\7@\2\2\67\6\3\2\2\289\7k\2\29:\7p\2\2:;\7r\2\2;<\7w\2\2<=\7"+
		"v\2\2=\b\3\2\2\2>?\7+\2\2?\n\3\2\2\2@A\7.\2\2A\f\3\2\2\2BC\7q\2\2CD\7"+
		"w\2\2DE\7v\2\2EF\7r\2\2FG\7w\2\2GH\7v\2\2H\16\3\2\2\2IJ\7-\2\2J\20\3\2"+
		"\2\2KL\7<\2\2L\22\3\2\2\2MN\7*\2\2N\24\3\2\2\2OP\7i\2\2PQ\7t\2\2QR\7q"+
		"\2\2RS\7w\2\2ST\7r\2\2T\26\3\2\2\2UV\7/\2\2VW\7]\2\2W\30\3\2\2\2XZ\t\2"+
		"\2\2YX\3\2\2\2Z[\3\2\2\2[Y\3\2\2\2[\\\3\2\2\2\\\32\3\2\2\2]_\t\2\2\2^"+
		"]\3\2\2\2_`\3\2\2\2`^\3\2\2\2`a\3\2\2\2ab\3\2\2\2bf\7\60\2\2ce\t\2\2\2"+
		"dc\3\2\2\2eh\3\2\2\2fd\3\2\2\2fg\3\2\2\2g\34\3\2\2\2hf\3\2\2\2ik\7\17"+
		"\2\2ji\3\2\2\2jk\3\2\2\2kl\3\2\2\2lm\7\f\2\2m\36\3\2\2\2np\t\3\2\2on\3"+
		"\2\2\2pq\3\2\2\2qo\3\2\2\2qr\3\2\2\2r \3\2\2\2st\7o\2\2tu\7q\2\2uv\7f"+
		"\2\2vw\7w\2\2wx\7n\2\2xy\7g\2\2y\"\3\2\2\2z{\7g\2\2{|\7p\2\2|}\7f\2\2"+
		"}~\7o\2\2~\177\7q\2\2\177\u0080\7f\2\2\u0080\u0081\7w\2\2\u0081\u0082"+
		"\7n\2\2\u0082\u0083\7g\2\2\u0083$\3\2\2\2\u0084\u0085\7u\2\2\u0085\u0086"+
		"\7v\2\2\u0086\u0087\7t\2\2\u0087\u0088\7c\2\2\u0088\u0089\7v\2\2\u0089"+
		"\u008a\7g\2\2\u008a\u008b\7i\2\2\u008b\u008c\7{\2\2\u008c&\3\2\2\2\u008d"+
		"\u008e\7o\2\2\u008e\u008f\7g\2\2\u008f\u0090\7t\2\2\u0090\u0091\7i\2\2"+
		"\u0091\u0092\7g\2\2\u0092(\3\2\2\2\u0093\u0094\7o\2\2\u0094\u0095\7w\2"+
		"\2\u0095\u0096\7n\2\2\u0096\u0097\7v\2\2\u0097\u0098\7k\2\2\u0098\u0099"+
		"\7o\2\2\u0099\u009a\7g\2\2\u009a\u009b\7t\2\2\u009b\u009c\7i\2\2\u009c"+
		"\u009d\7g\2\2\u009d*\3\2\2\2\u009e\u009f\7h\2\2\u009f\u00a0\7w\2\2\u00a0"+
		"\u00a1\7u\2\2\u00a1\u00a2\7g\2\2\u00a2,\3\2\2\2\u00a3\u00a4\7k\2\2\u00a4"+
		"\u00a5\7p\2\2\u00a5\u00a6\7k\2\2\u00a6\u00a7\7v\2\2\u00a7.\3\2\2\2\u00a8"+
		"\u00ac\t\4\2\2\u00a9\u00ab\t\5\2\2\u00aa\u00a9\3\2\2\2\u00ab\u00ae\3\2"+
		"\2\2\u00ac\u00aa\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\60\3\2\2\2\u00ae\u00ac"+
		"\3\2\2\2\t\2[`fjq\u00ac";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}