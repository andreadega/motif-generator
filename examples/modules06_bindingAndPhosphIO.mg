
module: input KA(kinase), output pA()
A() -[ KA(kinase) ]-> pA()
pA() -[ PA() ]-> A()
endmodule

module: input B(kinase), output B(kinase)
B(kinase) + C() -> D()
D() -> B(kinase) + C()
endmodule

init KA(kinase) 1
init A() 1
init PA() 1
init B(kinase) 10
init C() 1

strategy merge: input B(kinase), output pA()
