
grammar motifGenerator;

//return LinkedList<Module>
code: line EOF                          # codeRule
    | line code                             # codeList
    ;

//return Module or null
line: SPACETABS? NEWLINE                                        # skipstuff
      | module SPACETABS? NEWLINE                               # moduleLine
      | STRATEGY SPACETABS strategynt SPACETABS? NEWLINE        # strategyLine
      | init SPACETABS? NEWLINE                                 # initLine
      ;

//return Module, here infer Set<Species> from Set<Reaction>
module: MODULESTART SPACETABS? ':' SPACETABS? 'input' SPACETABS complex SPACETABS? ',' SPACETABS? 'output' SPACETABS complex SPACETABS? NEWLINE reactions NEWLINE MODULEEND # moduleRuleIO
      | MODULESTART SPACETABS? NEWLINE reactions NEWLINE MODULEEND # moduleRule
      | MODULESTART SPACETABS? 'group' SPACETABS? ID SPACETABS? NEWLINE reactions NEWLINE MODULEEND # moduleRuleGroup
      ;

//return Set<Reaction>
reactions: reaction                                  # reactionRule
            | reaction SPACETABS? NEWLINE reactions  # reactionList
            ;

//return Reaction
reaction: complex SPACETABS? '-[' SPACETABS? complex SPACETABS? ']->' SPACETABS? complex    # enzymaticReaction
          | complex SPACETABS? '+' SPACETABS? complex SPACETABS? '->' SPACETABS? complex    # bindingReaction
          | complex SPACETABS? '->' SPACETABS? complex SPACETABS? '+' SPACETABS? complex    # unbindingReaction
          | complex SPACETABS? '->' SPACETABS? complex                                      # conversionReaction
          ;

//return Complex
complex: species                      # complexRule
         | species complex            # complexList
         ;

//return Species
species: ID '(' SPACETABS? idList SPACETABS? ')'   # speciesRule
       | ID '(' SPACETABS? ')'   # speciesRuleEmptyProperties
       ;

//return Set<String>
idList: ID                                          # idListTerm
      | ID SPACETABS? ',' SPACETABS? idList         # idListList
      ;

//return String      
strategynt: FUSE        # fuseToken
          | MERGE       # mergeToken
          | MULTIMERGE       # multimergeToken
          | MERGE SPACETABS? ':' SPACETABS? 'input' SPACETABS complex SPACETABS? ',' SPACETABS? 'output' SPACETABS complex      # mergeTokenIO          
          ;

init: INIT SPACETABS complex SPACETABS INT                     # initINT
      | INIT SPACETABS complex SPACETABS DOUBLE                 # initDOUBLE
      ;

INT                   : [0-9]+ ;
DOUBLE                : [0-9]+ '.' [0-9]* ;
NEWLINE               : '\r'? '\n';
SPACETABS             : [ \t]+ ;
MODULESTART           : 'module' ;
MODULEEND             : 'endmodule' ;
STRATEGY              : 'strategy' ;
MERGE                 : 'merge' ;
MULTIMERGE            : 'multimerge' ;
FUSE                  : 'fuse' ;
INIT                  : 'init' ;
ID                    : [a-zA-Z][a-zA-Z0-9\_]* ;
//STRING                : '"' .*? '"' ;
