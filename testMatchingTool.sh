#!/bin/bash

mkdir -p classes

javac -Xlint -d classes src/motifGenerator/Pair.java src/motifGenerator/Species.java src/motifGenerator/MatchingTool.java

java -cp classes motifGenerator.MatchingTool

cd classes
rm -rf org
rm -rf com
rm -rf motifGenerator
rm -rf antlr
cd ..



