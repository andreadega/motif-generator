
package motifGenerator;

import com.scottlogic.util.*;
import java.util.*;

public class Complex {

  private SortedList<Species> sortedList;
  //private double initialConc = 0.0;
  
  public Complex(){
    sortedList = new SortedList<Species>(new SpeciesComparator());
  }
  
  /**
   * Copy constructor, no deep cloning of Species, yet copies the SortedList
   * */
  
  public Complex(Complex c){
    sortedList = new SortedList<Species>(new SpeciesComparator());
    sortedList.addAll(c.getMembers());
    //~ this.initialConc = c.initialConc;
  }
  
  public String toString(){
    String returnString = "";
    Iterator<Species> ite = sortedList.iterator();
    while (ite.hasNext()){
      Species current = ite.next();
      returnString += current.toString();
    }
    return returnString;
  }

  public String toStringIDformat(){
    boolean first = true;
    String returnString = "";
    Iterator<Species> ite = sortedList.iterator();
    while (ite.hasNext()){
      Species current = ite.next();
      if (first) {
        returnString += current.toStringIDformat();
        first = false;
      }else{
        returnString += "_" + current.toStringIDformat();
      }
    }
    return returnString;
  }
  
  /**
   * add Species s to sortedList
   * false when give object is null
   * */
  
  public boolean add(Species s){
    return sortedList.add(s);
  }
  
  public Iterator<Species> getMembersIterator(){
    return sortedList.iterator();
  }
  
  public SortedList<Species> getMembers(){
    return sortedList;
  }
  
  @Override
  public boolean equals(Object other){
    if (other == null) return false;
    if (other == this) return true;
    if (!(other instanceof Complex))return false;
    Complex c = (Complex)other;  //downcast
    
    //check that the size is the same
    if (this.sortedList.size() != c.getMembers().size()) return false;
    
    //check that the elements are the same (the are ordered, which makes
    //it easy to check pairwise iterating the lists)
    Iterator<Species> i1 = this.sortedList.iterator();
    Iterator<Species> i2 = c.getMembersIterator();
    while (i1.hasNext() && i2.hasNext()){
      if (! i1.next().equals(i2.next())) return false;
    }
    //no differences found
    return true;
  }
  
  @Override
  public int hashCode(){
    int hash = 0;
    Iterator<Species> ite = this.sortedList.iterator();
    while(ite.hasNext()){
      hash += ite.next().hashCode();
    }
    return hash;
  }   
  
  public boolean hasMember(Species s){
    return this.sortedList.contains(s);
  }
  
  /**
   * for every instance of oldS in sortedList, substitute oldS with newS
   * */
  
  public void substituteMember(Species oldS, Species newS){
    //remove and count how many copies were removed
    int nRemoved = 0;
    while (this.sortedList.remove(oldS)){ //if removed returns true
      nRemoved++;
    }
    //add as many newS as removed oldS
    for (int i = 0; i<nRemoved; i++) this.sortedList.add(newS);
  }
  
  //~ public double getInitialConc(){
    //~ return initialConc;
  //~ }
  //~ 
  //~ public void setInitialConc(double initialConc){
    //~ this.initialConc = initialConc;
  //~ }
  
  
}
