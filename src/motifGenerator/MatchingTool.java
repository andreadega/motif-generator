
package motifGenerator;

import java.util.*;

public class MatchingTool {
  
  private static boolean debug = false;
  
  /**
   * Method that takes as input a set of pairs. Each pair (A,B) is a
   * Pair object with A and B Species objects. The first and second elements
   * of the pair are assumed to belong to two disjoint sets.
   * The method returns a set, which is the enumeration of all maximal matches.
   * A maximal match is a set of pairs where each species is mentioned
   * only once and it is not possible to add other pairs from the initial
   * set without repeating at least one species.
   * For example, given the initial set of pairs {(A,B),(A,C),(D,C)}
   * then the set of all maximal matches is {(A,B),(D,C)},{(A,C)}
   * Notice that the matches are maximal, not necessarily maximum. The
   * maximum match is {(A,B),(D,C)}, because has the largest possible
   * number of pairs without repeating speacies. The match {(A,C)} is not
   * maximum, nonetheless it is maximal, because no other pairs can be 
   * added without repetition.
   * */
  
  public static Set<Set<Pair<Species,Species>>> getMaximalMatches(Set<Pair<Species,Species>> setOfPairs){
    
    //to return
    Set<Set<Pair<Species,Species>>> setOfMaximalMatches = new HashSet<Set<Pair<Species,Species>>>();
    //to use in the recursion
    Set<Set<Pair<Species,Species>>> newSetOfMatches = new HashSet<Set<Pair<Species,Species>>>();
     
    //convert to map
    Map<Species,Set<Species>> map = convertToMap(setOfPairs);
    
    //for all species (left-hand-side of the pairs in the original setOfPairs)
    Iterator<Species> ite = map.keySet().iterator();
    while (ite.hasNext()){
      Species lSpecies = ite.next();
      //try to augment the lists in setOfMaximalMatches
      
      //list of right-hand-side species for the current left-hand-side species
      Set<Species> rightHS = map.get(lSpecies);
      
      if (setOfMaximalMatches.isEmpty()){
        //base of the recursion
        
        //add all possible pairs as separate matchings
        Iterator<Species> ite2 = rightHS.iterator();
        while (ite2.hasNext()){
          Species rSpecies = ite2.next();
          Set<Pair<Species,Species>> list = new HashSet<Pair<Species,Species>>();
          list.add(new Pair<Species,Species>(lSpecies,rSpecies));
          setOfMaximalMatches.add(list);
        }
        if (debug) System.out.println("Initial set of matches: " + setOfMaximalMatches.toString());
        
      }else{
        //inductive step of the recursion
        
        //for all current maximal matches
        Iterator<Set<Pair<Species,Species>>> ite2 = setOfMaximalMatches.iterator();
        while (ite2.hasNext()){
          Set<Pair<Species,Species>> matchSet = ite2.next();
          
          //for all rhs of the current lhs
          Iterator<Species> iteRHS = rightHS.iterator();
          //keep track about whether the current match (matchSet) is
          //augmented with the new matched pair (lSpecies,rSpecies)
          //if all rSpecies in rightHS are already matched then matchSet
          //cannot be augmented and needs to be passed on to the next iteration
          //(i.e. the next lSpecies)
          boolean augmantationOccurred = false;
          while (iteRHS.hasNext()){
            Species rSpecies = iteRHS.next();
            
            //find whether the rSpecies is already matched, and if so 
            //create a new match substituting the current matched pair
            //that contains rSpecies with the new matched pair (lSpecies,rSpecies)
            Set<Pair<Species,Species>> sub = attemptSubstitution(lSpecies,rSpecies,matchSet);


            if (sub == null){
              //if sub is null, then no substitution took place, and so
              //rSpecies is not already matched and so we can create a new
              //match by augmenting the current one with (lSpecies,rSpecies)
              
              Set<Pair<Species,Species>> aug = new HashSet<Pair<Species,Species>>(matchSet);
              aug.add(new Pair<Species,Species>(lSpecies,rSpecies));
              augmantationOccurred=true;
              //add the new match to the set of new matches to bring forward to
              //the next iteration on lSpecies
              if (debug) System.out.println("add aug to tmp: " + aug.toString());
              addMatch(aug,newSetOfMatches);
              if (debug) System.out.println("tmp: " + newSetOfMatches.toString());
            }else{
              //substitution took place, add the new match to the set of new matches to bring forward to
              //the next iteration on lSpecies
              if (debug) System.out.println("add sub to tmp: " + sub.toString());
              addMatch(sub,newSetOfMatches);
              if (debug) System.out.println("tmp: " + newSetOfMatches.toString());
            }
          }
          //If after iterating through all the rSpecies there was no
          //augmentation of matchSet, then bring it forward, that is
          //add it to the set of new matches to bring it forward to
          //the next iteration on lSpecies
          if(!augmantationOccurred) {
            if (debug) System.out.println("add from previous to tmp: " + matchSet.toString());
            addMatch(matchSet,newSetOfMatches);
            if (debug) System.out.println("tmp: " + newSetOfMatches.toString());
          }
        } //done iterating match sets
        //update the setOfMaximalMatches
        setOfMaximalMatches.clear();
        setOfMaximalMatches.addAll(newSetOfMatches);
        newSetOfMatches.clear(); // prepare for next recursive step
        if (debug) System.out.println("Updated set of matches: " + setOfMaximalMatches.toString());
      } //done else case
      
    } //done iterating LHS (key of map)
    
    
    return setOfMaximalMatches;
  }
  
  /**
   * Attempt to add a new match to a set of matches. As we aim to obtain
   * the set of maximal matches, then if the new match is contained in
   * a match that is already in the set, then it should not be added.
   * At the same time, if the new match contains a match already in the 
   * set, this latter should be removed and the new added.
   * If none of the above, just add the new match to the set.
   * */
  
  private static void addMatch(Set<Pair<Species,Species>> newMatch,Set<Set<Pair<Species,Species>>> newSetOfMatches){
    //need a copy as I'm going to alter the Set I'm iterating on
    Set<Set<Pair<Species,Species>>> copy = new HashSet<Set<Pair<Species,Species>>>(newSetOfMatches);
    
    Iterator<Set<Pair<Species,Species>>> ite = copy.iterator();
    while (ite.hasNext()){
      Set<Pair<Species,Species>> match = ite.next();
      if (match.containsAll(newMatch)){
        //the new match is a subset of an existing match, don't add and return
        return;        
      }else if (newMatch.containsAll(match)){
        //the new match contains an existing match remove the existing match and
        //add the new match. Not sure if other matches might be contained in newMatch
        //so continue
        newSetOfMatches.remove(match);
        //newSetOfMatches.add(newMatch); // commented as we add at the end
      }else{
        //do nothing
      }
    }
    //just add the new match
    newSetOfMatches.add(newMatch);
  }
  
  /**
   * Search in the given match whether exists a pair (A,B) in match such that
   * the Species sRHS is in the RHS (i.e. sRHS=B).
   *  If so, create a new match, as a copy of the given match, removing 
   * the pair (A,B) and adding the new pair (sLHS,sRHS). Return the new match
   * or null if such (A,B) pair does no exists
   * */
  
  private static Set<Pair<Species,Species>> attemptSubstitution(Species sLHS,Species sRHS, Set<Pair<Species,Species>> match){
    Set<Pair<Species,Species>> returnMatch = null;
    
    Iterator<Pair<Species,Species>> ite = match.iterator();
    while (ite.hasNext()){
      Pair<Species,Species> pair = ite.next();
      if (pair.second.equals(sRHS)){
        //sRHS already matched to another species, create new match and perform substitution
        returnMatch = new HashSet<Pair<Species,Species>>(match);
        returnMatch.remove(pair);
        returnMatch.add(new Pair<Species,Species>(sLHS,sRHS));
        //substitution done, so return (by def. no other sub possible)
        return returnMatch;
      }
    }
    return returnMatch;
  }
  
  /**
   * Convert a set of Pairs into a map of species -> set of species
   * This method assumes that each pair (A,B) in the set is a
   * Pair object with A and B Species objects. The first and second elements
   * of the pair are assumed to belong to two disjoint sets
   * The keys of the map belong to the set of the species on the left
   * hand side of the pairs.
   * For example {(A,B),(A,C),(D,C)} becomes the map {A->{B,C},D->{C}}
   * */
  
  private static Map<Species,Set<Species>> convertToMap(Set<Pair<Species,Species>> setOfPairs){
    Map<Species,Set<Species>> returnMap = new HashMap<Species,Set<Species>>();
    
    Iterator<Pair<Species,Species>> ite = setOfPairs.iterator();
    while (ite.hasNext()){
      Pair<Species,Species> tmp = ite.next();
      if (returnMap.containsKey(tmp.first)){
        //add to existing mapping
        Set<Species> s = returnMap.get(tmp.first);
        s.add(tmp.second);
      }else{
        //add new mapping
        Set<Species> s = new HashSet<Species>();
        s.add(tmp.second);
        returnMap.put(tmp.first,s);
      }
    }    
    
    return returnMap;
  }
  
  /**
   * find all species on the left hand side of the pairs
   * For example {(A,B),(A,C),(D,C)} returns the set {A,D}
   * */
  
  private static Set<Species> getSpeciesLeftHS(Set<Pair<Species,Species>> setOfPairs){
    Set<Species> returnSet = new HashSet<Species>();
    
    Iterator<Pair<Species,Species>> ite = setOfPairs.iterator();
    while (ite.hasNext()){
      Pair<Species,Species> tmp = ite.next();
      returnSet.add(tmp.first);
    }
    
    return returnSet;
  }
  
  /**
   * 
   * Main used for testing and debugging
   * */
  public static void main(String[] args) {
    
    //MatchingTool.debug = true;
    
    Set<Pair<Species,Species>> setOfPairs = new HashSet<Pair<Species,Species>>();
    Set<String> emptyProperties = new HashSet<String>();
    Species a = new Species("A",emptyProperties);
    Species b = new Species("B",emptyProperties);
    Species c = new Species("C",emptyProperties);
    Species d = new Species("D",emptyProperties);
    Species e = new Species("E",emptyProperties);
    Species f = new Species("F",emptyProperties);
    Species g = new Species("G",emptyProperties);
    Species h = new Species("H",emptyProperties);
    Species i = new Species("I",emptyProperties);
    setOfPairs.add(new Pair<Species,Species>(a,e));
    setOfPairs.add(new Pair<Species,Species>(b,e));
    setOfPairs.add(new Pair<Species,Species>(b,f));
    setOfPairs.add(new Pair<Species,Species>(b,g));
    setOfPairs.add(new Pair<Species,Species>(c,g));
    setOfPairs.add(new Pair<Species,Species>(d,g));
    setOfPairs.add(new Pair<Species,Species>(d,h));
    setOfPairs.add(new Pair<Species,Species>(d,i));
    System.out.println("Initial set of pairs: " + setOfPairs.toString());
    Set<Set<Pair<Species,Species>>> mm = getMaximalMatches(setOfPairs);
    System.out.println("Maximal matches: " + mm.toString());
  }
}
