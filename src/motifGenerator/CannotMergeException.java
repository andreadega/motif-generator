
package motifGenerator;

public class CannotMergeException extends Exception {
  
  private static final long serialVersionUID = 1;
  
  public CannotMergeException(String msg){
    super(msg);
  }
}
