
module
A(ik) -[ KA(ak) ]-> pA(ak)
pA(ak) -[ PA(ap) ]-> A(ik)
endmodule

module
B(ik,ik2) + C(ak,ak2) -> BC(ak2,ik2)
BC(ik2,ak2) -> B(ik,ik2) + C(ak,ak2)
endmodule

module
D(ik2) -[ KD(ak) ]-> pD(ak2)
pD(ak2) -[ PD(ap) ]-> D(ik2)
endmodule

strategy merge
